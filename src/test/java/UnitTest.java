/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.unit.Trader;
import com.lordbobstein.island.model.entities.unit.Unit;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lordbobstein
 */
public class UnitTest
{
    public UnitTest()
    {
    }   
    
    @Test
    public void shouldReturn138dot60()
    {
        Unit unit = new Trader();
        unit.setCurrentLocation(new Location(1, 1));
        unit.setDestinationLocation(new Location(99, 99));
        
        double result = 138.60;
        double calculateDistance = unit.calculateDistanceFromCurrentLocation();
        assertEquals(result, calculateDistance, 0);
    }
    
    @Test
    public void shouldReturn0()
    {
        Unit unit = new Trader();
        unit.setCurrentLocation(new Location(1, 1));
        unit.setDestinationLocation(new Location(1, 1));
        
        double shouldReturn = 0;
        double calculateDistance = unit.calculateDistanceFromCurrentLocation();
        assertEquals(shouldReturn, calculateDistance, 0);    
    }
    
    @Test
    public void shouldReturn1()
    {
        Unit unit = new Trader();
        unit.setCurrentLocation(new Location(2, 1));
        unit.setDestinationLocation(new Location(1, 1));
        
        double shouldReturn = 1;
        double calculateDistance = unit.calculateDistanceFromCurrentLocation();
        assertEquals(shouldReturn, calculateDistance, 0);    
    }
    
    
    @Test
    public void shouldReturn0RequiredStages()
    {
        Unit unit = new Trader();
        unit.setCurrentLocation(new Location(1, 1));
        unit.setDestinationLocation(new Location(1, 1));
        
        int shouldReturn = 0;
        int requiredStages = unit.calculateRequiredStages();
        assertEquals(shouldReturn, requiredStages);  
    }
    
    @Test
    public void shouldReturn1RequiredStages()
    {
        Unit unit = new Trader();
        unit.setCurrentLocation(new Location(2, 1));
        unit.setDestinationLocation(new Location(1, 1));
        
        int shouldReturn = 1;
        int requiredStages = unit.calculateRequiredStages();
        assertEquals(shouldReturn, requiredStages);  
    }
    
    @Test
    public void shouldReturn14RequiredStages()
    {
        Unit unit = new Trader();
        unit.setCurrentLocation(new Location(1, 1));
        unit.setDestinationLocation(new Location(99, 99));
        
        int shouldReturn = 14;
        int requiredStages = unit.calculateRequiredStages();
        assertEquals(shouldReturn, requiredStages);  
    }
    
    @Test
    public void shouldReturn3RequiredStages()
    {
        Unit unit = new Trader();
        unit.setCurrentLocation(new Location(1, 1));
        unit.setDestinationLocation(new Location(17, 17));
        
        int shouldReturn = 3;
        int requiredStages = unit.calculateRequiredStages();
        assertEquals(shouldReturn, requiredStages);  
    }
    
}
