<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<tiles:insertDefinition name="homeTemplate">
    <tiles:putAttribute name="body">
        <!-- Content Section -->
        <header class="head">
            <section>
                <div class="container">
                    <div class="row" >
                        <div class="col-md-6 col-md-offset-3">
                            <fieldset >                   
                                <legend>Zarejestruj się:</legend>
                                <sf:form method="POST" modelAttribute="playerData">

                                    <label for="username" class="login">Login</label>                                  
                                    <sf:input path="login" id="username" class="login"/>
                                    <br />
                                    <sf:errors path="login"class="error"/>

                                    <br />
                                    <label for="password" class="login">Hasło</label>
                                    <sf:password path="password" id="password"class="login"/>
                                    <br />
                                    <sf:errors path="password"class="error"/>
                                    <br />                                    
                                    <input type="submit" value="Rejestruj" class="login">
                                    <br />  
                                    <small class="error">${error}</small>
                                </sf:form>
                                <br />                        
                            </fieldset>
                        </div>
                    </div>  
                </div>
        </header>
    </tiles:putAttribute>
</tiles:insertDefinition>