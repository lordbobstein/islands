<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="homeTemplate">
    <tiles:putAttribute name="body">

        <!-- Header -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <img class="img-responsive" src="${pageContext.request.contextPath}/resources/profile.png" alt="">
                        <div class="intro-text">
                            <span class="name">Islands - gra MMOSG</span>
                            <hr class="star-light">
                            <span class="skills">Żywioł - Strategia - Potęga</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>Zrzuty ekranu</h2>
                        <hr class="star-primary">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 portfolio-item">
                        <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="${pageContext.request.contextPath}/resources/1.png" class="img-responsive" alt="">
                        </a>
                    </div>
                    <div class="col-sm-4 portfolio-item">
                        <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="${pageContext.request.contextPath}/resources/2.png" class="img-responsive" alt="">
                        </a>
                    </div>
                    <div class="col-sm-4 portfolio-item">
                        <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img src="${pageContext.request.contextPath}/resources/3.png" class="img-responsive" alt="">
                        </a>
                    </div>

                </div>
            </div>
        </section>

        <!-- About Section -->
        <section class="success" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>O grze</h2>
                        <hr class="star-light">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-2">
                        <p align="justify">Ahoj! Chcesz przeżyć morską przygodę? Wskakuj na pokład! Wpierw jednak zapatrz się w niezbędne surowce, zbuduj jednostki i zgromadź pożywienie dla swojej załogi!</p>
                    </div>
                    <div class="col-lg-4">
                        <p align="justify">Prezentujemy niezwykłą grę, w której w czasie jednej rozgrywki możesz zostać wladcą całego archipelagu! Obież własną strategię, rozplanuj kolejne działania i walcz o zwycięstwo!</p>
                    </div>
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <spring:url value="/auth/register" var="regUrl" htmlEscape="true"/>                        
                        <a href="${regUrl}" class="btn btn-lg btn-outline">
                            <i class="fa fa-download"></i> Zarejestruj się!
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </tiles:putAttribute>
</tiles:insertDefinition>