<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="homeTemplate">
    <tiles:putAttribute name="body">
        <!-- Content Section -->

        <header class="head">
            <div class="container">
                <div class="row" >
                    <div class="col-md-6 col-md-offset-3">
                        <fieldset >                   
                            <legend>Zaloguj się:</legend>
                            <form method="post" action="" >

                                <label for="username" class="login">Login</label>
                                <input type="text" id="username" name="username" class="login"/>

                                <br />
                                <label for="password" class="login">Hasło</label>
                                <input type="password" id="password" name="password" class="login">
                                <br />                                    
                                <input type="submit" value="Zaloguj" class="login">
                                <br />  
                                <small class="error">${error}</small>
                            </form>
                            <br />
                            <div class="register"><a href="/island/auth/register">Zarejestruj się</a></div>                            
                        </fieldset>
                    </div>
                </div>  
            </div>
        </header>
                            
    </tiles:putAttribute>
</tiles:insertDefinition>