<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<tiles:insertDefinition name="dashboardTemplate">

    <tiles:putListAttribute name="scripts">
        <tiles:addAttribute value="resultsScripts.js" />       
    </tiles:putListAttribute> 

    <tiles:putAttribute name="body">
        <!-- Content Section -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Ranking</div>
                    <div class="panel-body">
                        <p>Poniżej zostały przedstawione aktualne rezultaty uzyskane przez graczy z tego archipelagu</p>
                    </div>

                    <!-- Table -->
                    <table class="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nazwa gracza</th>
                            <th>Ilość wysp</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: playerList">
                        <tr>
                            <td data-bind="text: id"></td>
                            <td data-bind="text: login"></td>
                            <td data-bind="text: numberOfIslands"></td>
                        </tr>
                    </tbody>
                </table>
                </div>
                
            </div>   
        </div>        
    </tiles:putAttribute>
</tiles:insertDefinition>