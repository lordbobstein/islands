<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<tiles:insertDefinition name="dashboardTemplate">

    <tiles:putListAttribute name="scripts">
        <tiles:addAttribute value="dashboardScripts.js" />      
    </tiles:putListAttribute> 

    <tiles:putAttribute name="body">

        <div id="page-wrapper" data-bind="with: statistics">
            <div class="container-fluid">
                <!-- SUROWCE -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Surowce:
                        </h1>
                    </div>
                </div>                
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa- fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge badge" data-bind="text: foodValue() ">110</div>
                                        <div>Jedzenie</div>
                                    </div>
                                </div>
                            </div>
                            <spring:url value="/dashboard/resourcedetails" var="foodUrl" htmlEscape="true">
                                <spring:param name="target" value="food" />
                            </spring:url>
                            <a href="${foodUrl}">
                                <div class="panel-footer">
                                    <span class="pull-left">Zobacz szczegóły</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa- fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge badge" data-bind="text: stoneValue() ">120</div>
                                        <div>Kamień</div>
                                    </div>
                                </div>
                            </div>
                            <spring:url value="/dashboard/resourcedetails" var="stoneUrl" htmlEscape="true">
                                <spring:param name="target" value="stone" />
                            </spring:url> 
                            <a href="${stoneUrl}">
                                <div class="panel-footer">
                                    <span class="pull-left">Zobacz szczegóły</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa- fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge badge" data-bind="text: woodValue() ">0</div>
                                        <div>Drzewo</div>
                                    </div>
                                </div>
                            </div>
                            <spring:url value="/dashboard/resourcedetails" var="woodUrl" htmlEscape="true">
                                <spring:param name="target" value="wood" />
                            </spring:url>
                            <a href="${woodUrl}">
                                <div class="panel-footer">
                                    <span class="pull-left">Zobacz szczegóły</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>                  
                </div>

                <!-- SUROWCE - END -->



                <!-- BUDNKI -->                   

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Statystyki:
                        </h1>
                    </div>
                </div>  
                <div class="row">                        
                    <div class="col-lg-12 col-md-12">
                        <spring:url value="/dashboard/buildings" var="buildingsUrl" htmlEscape="true" />                              
                        <a href="${buildingsUrl}">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">                                        
                                        <div class="text-left buildings-info dashboard-panel-text">
                                            <div class="col-xs-3 h2">Ilość budynków:</div>
                                            <div class="col-xs-3 huge badge" data-bind="text: numberOfBuildings">4</div>
                                        </div>
                                        <div class="text-right dashboard-panel-text">                                
                                            <div class="h4 col-xs-3">Czy wybrano budynek do zbudowania w tej turze:</div>
                                            <div class="huge col-xs-3 badge" data-bind="if: isBuildingInQueue">Tak</div>
                                            <div class="huge col-xs-3 badge" data-bind="ifnot: isBuildingInQueue">Nie</div>
                                        </div>
                                    </div>
                                </div>                    
                            </div>
                        </a>
                    </div> 
                </div> 

                <!-- BUDNKI- END -->
                <!-- JEDNOSTKI -->

                <div class="row">                   
                    <div class="col-lg-12 col-md-12">
                        <spring:url value="/dashboard/units" var="unitsUrl" htmlEscape="true" />  
                        <a href="${unitsUrl}">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">                                        
                                        <div class="text-left buildings-info dashboard-panel-text">                              
                                            <div class="col-xs-3 h2">Ilość okrętów:</div>                                            
                                            <div class="col-xs-3 huge badge" data-bind="text: numberOfUnits">43</div>
                                        </div>
                                    </div>
                                </div>                    
                            </div>
                        </a>
                    </div>                      
                </div>
                <!-- JEDNOSTKI - END-->

                <!-- WYSPY -->

                <div class="row">                   
                    <div class="col-lg-12 col-md-12">
                        <spring:url value="/dashboard/results" var="resultsUrl" htmlEscape="true" />  
                        <a href="${resultsUrl}">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">                                        
                                        <div class="text-left buildings-info dashboard-panel-text">                                
                                            <div class="col-xs-3 h2">Ilość wysp:</div>                                             
                                            <div class="col-xs-3 huge badge" data-bind="text: numberOfIslands">2</div>
                                        </div>
                                    </div>
                                </div>                    
                            </div>
                        </a>
                    </div>                      
                </div>
                <!-- WYSPY -END -->                     

            </div>
        </div>   
        <!-- /#page-wrapper -->

    </tiles:putAttribute>
</tiles:insertDefinition>