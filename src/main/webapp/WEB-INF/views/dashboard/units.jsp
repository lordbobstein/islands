<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<tiles:insertDefinition name="dashboardTemplate">
    <tiles:putListAttribute name="styles">        
        <tiles:addAttribute value="units.css" />        
    </tiles:putListAttribute> 

    <tiles:putListAttribute name="scripts">
        <tiles:addAttribute value="unitsScripts.js" />  
        <tiles:addAttribute value="knockout-jqueryui.js" />  
    </tiles:putListAttribute> 

    <tiles:putAttribute name="body">
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Stocznia-->

                <h2>Stocznia</h2>
                <div class="row">
                    <div data-bind="foreach: shipTypes">
                        <div class="panel panel-default col-lg-6">
                            <div class="panel-heading">                            
                                <h3 class="panel-title text-center" data-bind="if: $index() === 0">Okręt handlowy</h3>
                                <h3 class="panel-title text-center" data-bind="if: $index() === 1">Okręt wojenny</h3>
                            </div>
                            <div class="panel-body">
                                <div data-bind="if: $index() === 0" class="text-center">Pozwala pozyskiwać surowce z innych wysp archipelagu, transportując je do portu</div>
                                <div data-bind="if: $index() === 1" class="text-center">Patroluje okolice wybranej wyspy starając się odeprzeć ataki</div>
                                <span data-bind="if: enoughResourcesToBuild"><button type="button" class="btn btn-success center-block" data-bind="click: $parent.buildUnit">Zbuduj</button></span>
                                <span data-bind="ifnot: enoughResourcesToBuild"><button type="button" class="btn btn-success center-block disabled">Nie posiadasz wystarczającej ilości surowców</button></span>
                                <table class="table text-center">
                                    <thead>
                                        <tr>
                                            <td>Jedzenie</td>
                                            <td>Kamień</td>
                                            <td>Drzewo</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-bind="text: foodCost"></td>
                                            <td data-bind="text: stoneCost"></td>
                                            <td data-bind="text: woodCost"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> 

                    </div>
                </div>

                <div id="accordion">
                    <!-- Okrety wojenne -->

                    <h3>Okręty wojenne</h3> 
                    <!--ko if: warshipsReady -->
                    <div>
                        <div data-bind="if: warships().length === 0">
                            <div  class="alert alert-info" role="alert" >
                                <b>Nie posiadasz żadnego okrętu wojennego</b>
                            </div>
                        </div >
                        <div data-bind="if: warships().length !== 0">
                            <div data-bind="tabs: { refreshOn: warships }" class="tabs">
                                <ul>                            
                                    <!-- ko foreach: warships -->
                                    <li data-bind="attr: { 'aria-controls': tabId }">  
                                        <a data-bind="attr: { title: 'Okręt wojenny o id ' + id(), href: href }, text: tabName"></a>  
                                    </li>  
                                    <!-- /ko -->
                                </ul>
                                <!-- ko foreach: warships -->
                                <div data-bind="attr: { id: tabId }">                            
                                    <div class="row">
                                        <div class="col-lg-6 panel panel-info panelRow1">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Kurs</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div data-bind="if: !inMove() && isInThePort()">
                                                    <span>Wybierz kurs:</span>
                                                    <form class="form-inline" role="form">
                                                        <div class="form-group"> 
                                                            <div class="input-group">                                                    
                                                                <select class="input-lg" data-bind="click: $parent.getPossibleTargets, options: possibletargets,
                                optionsText: function(item) {
                                    return 'Tury: ' + item.requiredStages + ' Surowiec: ' + item.islandResourcesType + ' Id:' + item.id;
                                },
                                value: selected,
                                optionsCaption: 'Wybierz...'">                   
                                                                </select>
                                                                <button type="button" class="btn btn-warning" data-bind="click: $parent.newCourse">OK</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div data-bind="if: inMove()">
                                                    <span>Okręt żegluje kursem na:</span> 
                                                    <span class="badge badge-center unitsInfoHeader pull-right" data-bind="text: destinationLocation"></span>
                                                    <br/>
                                                    <span>Na wskazanej wyspie znajduje się:</span> 
                                                    <span class="badge badge-center unitsInfoHeader pull-right" data-bind="text: resourceType"></span>
                                                </div>
                                                <div data-bind="if: !inMove() && !isInThePort()">
                                                    <span>Na tej wyspie znajduje się:</span> 
                                                    <span class="badge badge-center unitsInfoHeader pull-right" data-bind="text: resourceType"></span>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 panel panel-info panelRow1">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Tury</h3>
                                            </div>
                                            <div class="panel-body">
                                                <span>Ilość tur do celu:</span><span class="badge badge-center unitsInfoHeader pull-right" data-bind="text: requiredStages"></span>
                                            </div>
                                        </div>   
                                    </div>                                
                                    <div class="row">                                
                                        <span data-bind="if: !inMove() && !isInThePort()"><button type="button" class="btn btn-success center-block" data-bind="click: $parent.return">Powrót do portu</button></span>
                                        <span data-bind="if: !inMove() && isInThePort()"><button type="button" class="btn btn-success center-block disabled">Okręt znajduje się w porcie</button></span>
                                        <span data-bind="if: inMove()"><button type="button" class="btn btn-success center-block disabled">W rejsie</button></span>
                                    </div>
                                </div> 
                                <!-- /ko -->
                            </div>
                        </div>
                    </div>
                    <!-- /ko -->

                    <!-- Okrety handlowe -->

                    <h3>Okręty handlowe</h3>
                    <!--ko if: tradersReady -->
                    <div>
                        <div data-bind="if: traders().length === 0">
                            <div  class="alert alert-info" role="alert" >
                                <b>Nie posiadasz żadnego okrętu handlowego</b>
                            </div>
                        </div>
                        <div data-bind="if: traders().length !== 0">
                            <div data-bind="tabs: { refreshOn: traders  }" class="tabs">
                                <ul>
                                    <!-- ko foreach: traders -->
                                    <li data-bind="attr: { 'aria-controls': tabId }">  
                                        <a data-bind="attr: { title: 'Okręt handlowy o id' + id(), href: href }, text: tabName"></a>  
                                    </li>  
                                    <!-- /ko -->
                                </ul>
                                <!-- ko foreach: traders -->
                                <div data-bind="attr: { id: tabId }">
                                    <div class="row">
                                        <div class="col-lg-6 panel panel-info panelRow1">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Kurs</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div data-bind="if: !inMove() && isInThePort()">
                                                    <span>Wybierz kurs:</span>
                                                    <form class="form-inline" role="form">
                                                        <div class="form-group"> 
                                                            <div class="input-group">                                                    
                                                                <select class="input-lg" data-bind="click: $parent.getPossibleTargets, options: possibletargets,
                                optionsText: function(item) {
                                    return 'Tury: ' + item.requiredStages + ' Surowiec: ' + item.islandResourcesType + ' Id:' + item.id;
                                },
                                value: selected,
                                optionsCaption: 'Wybierz...'">                   
                                                                </select>
                                                                <button type="button" class="btn btn-warning" data-bind="click: $parent.newCourse">OK</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div data-bind="if: inMove()">
                                                    <span>Okręt żegluje kursem na:</span> 
                                                    <span class="badge badge-center unitsInfoHeader pull-right" data-bind="text: destinationLocation"></span>
                                                    <br/>
                                                    <span>Pozyskiwany surowiec:</span> 
                                                    <span class="badge badge-center unitsInfoHeader pull-right" data-bind="text: resourceType"></span>
                                                </div>
                                                <div data-bind="if: !inMove() && !isInThePort()">
                                                    <span>Na tej wyspie znajduje się:</span> 
                                                    <span class="badge badge-center unitsInfoHeader pull-right" data-bind="text: resourceType"></span>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 panel panel-info panelRow1">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Tury</h3>
                                            </div>
                                            <div class="panel-body">
                                                <span>Ilość tur do celu:</span><span class="badge badge-center unitsInfoHeader pull-right" data-bind="text: requiredStages"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Surowce</h3>
                                            </div>
                                            <div class="panel-body">
                                                <span>Pozyskiwany surowiec:</span ><span class="badge unitsInfoHeader pull-right" data-bind="text: resourceType"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Ładownia</h3>
                                            </div>
                                            <div class="panel-body">
                                                <span class=" text-left">Wypełnienie ładowni:</span><span class="badge unitsInfoHeader pull-right" data-bind="text: hold"></span>
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="row">                                
                                        <span data-bind="if: !inMove() && !isInThePort()"><button type="button" class="btn btn-success center-block" data-bind="click: $parent.return">Powrót do portu</button></span>
                                        <span data-bind="if: !inMove() && isInThePort()"><button type="button" class="btn btn-success center-block disabled">Okręt znajduje się w porcie</button></span>
                                        <span data-bind="if: inMove()"><button type="button" class="btn btn-success center-block disabled">W rejsie</button></span>
                                    </div>
                                </div> 
                                <!-- /ko -->
                            </div>
                        </div>
                    </div>


                    <!-- /ko -->
                </div>
            </div>

        </tiles:putAttribute>
    </tiles:insertDefinition>