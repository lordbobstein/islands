<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>



<tiles:insertDefinition name="dashboardTemplate">

    <tiles:putListAttribute name="styles">        
        <tiles:addAttribute value="buildings.css" />        
    </tiles:putListAttribute> 

    <tiles:putListAttribute name="scripts">
        <tiles:addAttribute value="buildingsScripts.js" />      
    </tiles:putListAttribute> 

    <tiles:putAttribute name="body">

        <div id="page-wrapper">
            <div class="container-fluid">
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1"><div class="glyphicon glyphicon-glass fa-4x"></div></a></li>
                        <li><a href="#tabs-2"><span class="glyphicon glyphicon-tower fa-4x"></span></a></li>
                        <li><a href="#tabs-3"><span class="glyphicon glyphicon-tree-conifer fa-4x"></span></a></li>
                    </ul>
                    <div id="tabs-1" data-bind="with: buildings()[0]">
                        <h1>FARMA</h1>
                        <p>Twoi pracownicy potrzebują żywności, by dla Ciebie budować, żeglować, walczyć.</p>

                        <span data-bind="if: built() && enoughResourcesToBuild() && !$parent.wasBuilt(), click: $parent.buildBuilding "><button type="button" class="btn btn-success center-block">Ulepsz</button></span>
                        <span data-bind="if: !built() && enoughResourcesToBuild() && !$parent.wasBuilt(), click: $parent.buildBuilding"><button type="button" class="btn btn-success center-block">Zbuduj</button></span>
                        <span data-bind="if: !enoughResourcesToBuild() && !$parent.wasBuilt()"><button type="button" class="btn btn-success center-block disabled">Brak surowców</button></span>
                        <span data-bind="if: $parent.wasBuilt()"><button type="button" class="btn btn-success center-block disabled">W tej rundzie zbudowałeś już budynek</button></span>

                        <div class="level h2">Poziom: <span data-bind="text: level"></span></div>

                        <div class="panel panel-default">                              
                            <div class="panel-heading">Koszty</div>
                            <div class="panel-body">
                                <p>Koszty budynków z poziomu na poziom rosną, ale razem z nimi także wydajność. Warto inwestować!</p>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Jedzenie</td>
                                        <td>Kamień</td>
                                        <td>Drzewo</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-bind="text: foodCost"></td>
                                        <td data-bind="text: stoneCost"></td>
                                        <td data-bind="text: woodCost"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="tabs-2" data-bind="with: buildings()[1]">
                        <h1>KAMIENIOŁOM</h1>
                        <p>Twoi pracownicy potrzebują żywności, by dla Ciebie budować, żeglować, walczyć.</p>

                        <span data-bind="if: built() && enoughResourcesToBuild() && !$parent.wasBuilt(), click: $parent.buildBuilding "><button type="button" class="btn btn-success center-block">Ulepsz</button></span>
                        <span data-bind="if: !built() && enoughResourcesToBuild() && !$parent.wasBuilt(), click: $parent.buildBuilding"><button type="button" class="btn btn-success center-block">Zbuduj</button></span>
                        <span data-bind="if: !enoughResourcesToBuild() && !$parent.wasBuilt()"><button type="button" class="btn btn-success center-block disabled">Brak surowców</button></span>
                        <span data-bind="if: $parent.wasBuilt()"><button type="button" class="btn btn-success center-block disabled">W tej rundzie zbudowałeś już budynek</button></span>

                        <div class="level h2">Poziom: <span data-bind="text: level"></span></div>

                        <div class="panel panel-default">                              
                            <div class="panel-heading">Koszty</div>
                            <div class="panel-body">
                                <p>Koszty budynków z poziomu na poziom rosną, ale razem z nimi także wydajność. Warto inwestować!</p>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Jedzenie</td>
                                        <td>Kamień</td>
                                        <td>Drzewo</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-bind="text: foodCost"></td>
                                        <td data-bind="text: stoneCost"></td>
                                        <td data-bind="text: woodCost"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="tabs-3" data-bind="with: buildings()[2]">
                        <h1>DRWAL</h1>
                        <p>Twoi pracownicy potrzebują żywności, by dla Ciebie budować, żeglować, walczyć.</p>

                        <span data-bind="if: built() && enoughResourcesToBuild() && !$parent.wasBuilt(), click: $parent.buildBuilding "><button type="button" class="btn btn-success center-block">Ulepsz</button></span>
                        <span data-bind="if: !built() && enoughResourcesToBuild() && !$parent.wasBuilt(), click: $parent.buildBuilding"><button type="button" class="btn btn-success center-block">Zbuduj</button></span>
                        <span data-bind="if: !enoughResourcesToBuild() && !$parent.wasBuilt()"><button type="button" class="btn btn-success center-block disabled">Brak surowców</button></span>
                        <span data-bind="if: $parent.wasBuilt()"><button type="button" class="btn btn-success center-block disabled">W tej rundzie zbudowałeś już budynek</button></span>

                        <div class="level h2">Poziom: <span data-bind="text: level"></span></div>

                        <div class="panel panel-default">                              
                            <div class="panel-heading">Koszty</div>
                            <div class="panel-body">
                                <p>Koszty budynków z poziomu na poziom rosną, ale razem z nimi także wydajność. Warto inwestować!</p>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Jedzenie</td>
                                        <td>Kamień</td>
                                        <td>Drzewo</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-bind="text: foodCost"></td>
                                        <td data-bind="text: stoneCost"></td>
                                        <td data-bind="text: woodCost"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>

    </tiles:putAttribute>
</tiles:insertDefinition>