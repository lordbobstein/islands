<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<tiles:insertDefinition name="dashboardTemplate">
    <tiles:putAttribute name="body">
        <!-- Content Section -->
        <div id="page-wrapper">
            
            <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            ${resource.name}
                        </h1>                       
                    </div>
                </div>
            <div class="container">
                <div class="row" >
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">${resource.value}</div>
                                        <div>Posiadane</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">${resource.increase}</div>
                                        <div>Przyrost</div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
