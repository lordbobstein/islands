<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<html>
    <head>        
        <title>Admin Console</title>
        <script src="${pageContext.request.contextPath}/resources/jquery-2.1.1.js"></script>

    </head>
    <body>

        <spring:url value="/auth/logout" var="logoutUrl" htmlEscape="true"/> 
        <a href="${logoutUrl}">Wylogowanie</a>
        <br/><br/>
        <strong>Ilość zarejestrowanych graczy:</strong> ${fn:length(playerList)}
        <br/><br/>
        <table style="border: 3px">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>LOGIN</th>
                    <th>TYP GRACZA</th>                
                    <th>JEDZENIE</th>
                    <th>DRZEWO</th>
                    <th>KAMIEN</th>
                    <th>ILOSC BUDYNKOW</th>
                    
                    <th>ILOSC JEDNOSTEK</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="p" items="${playerList}"> 
                    <tr>
                        <td>${p.playerData.id}</td>
                        <td>${p.playerData.login}</td>
                        <td>${p.playerData.playerType}</td>
                        <td>${p.food.resourceValue}</td>
                        <td>${p.wood.resourceValue}</td>
                        <td>${p.stone.resourceValue}</td>
                        <td>${fn:length(p.buildings)}</td>
                    
                        <td>${fn:length(p.units)}</td>

                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <br/><br/>

        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>LICZBA WYSP</th>
                    <th>TURA</th>                
                </tr>
            </thead>
            <tbody>
                <c:forEach var="p" items="${archipelagosList}"> 
                    <tr>
                        <td>${p.id}</td>
                        <td>${p.numberOfIslands}</td>
                        <td>${p.stage}</td>   
                    </tr>
                </c:forEach>
            </tbody>
        </table>


        <br/><br/>
        <button id="newRoundButton">Nowa runda</button><input id="arch" type="text"/><br/>
        <button id="pauseAll">Pauza wszystkich</button><br/>
        <button id="startAll">Start wszystkich</button><br/>
        <button id="getTime">Pobierz czas</button><input id="archt" type="text"/><br/>
        Czas obecny:
        <%= new java.util.Date() %>
        <br/>
        Bedzie:
        <span id="time"></span>


        <script>
            $("#newRoundButton").click(function ()
            {
                var id = $("#arch").val();
                $.post("${pageContext.request.contextPath}/admin/newround", {archipelagoId: id});
            });

            $("#pauseAll").click(function ()
            {
                $.post("${pageContext.request.contextPath}/admin/pauseall");
            });
            
            $("#startAll").click(function ()
            {
                $.post("${pageContext.request.contextPath}/admin/startall");
            });

            $("#getTime").click(function ()
            {
                var id = $("#archt").val();
                $.post("${pageContext.request.contextPath}/admin/gettime", {archipelagoId: id}, function(data)
                {
                    $("#time").html(data);                    
                });
            });

        </script>

    </body>
</html>
