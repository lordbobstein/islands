<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="collapse navbar-collapse navbar-ex1-collapse">    
    <ul class="nav navbar-nav side-nav"> 
        <li class="active">
            <spring:url value="/dashboard/index" var="homeUrl" htmlEscape="true"/>
            <a href="${homeUrl}">
                <ul>
                    <li class="menu_links"><div>WYSPA</div></li>
                    <li class="menu_links"></li>                                
                </ul>
            </a>
        </li>
        <li class="active">
            <spring:url value="/dashboard/buildings" var="buildingsUrl" htmlEscape="true"/>
            <a href="${buildingsUrl}">
                <ul>
                    <li class="menu_links"><div>BUDYNKI</div></li>
                    <li class="menu_links"></li>                                
                </ul>
            </a>
        </li>
        <li class="active">
            <spring:url value="/dashboard/units" var="unitsUrl" htmlEscape="true"/>
            <a href="${unitsUrl}">
                <ul>
                    <li class="menu_links"><div>OKRĘTY</div></li>
                    <li class="menu_links"></li>                                
                </ul>
            </a>
        </li>
        <li class="active">
            <spring:url value="/dashboard/results" var="resultsUrl" htmlEscape="true"/>
            <a href="${resultsUrl}">
                <ul>
                    <li class="menu_links"><div>RANKING</div></li>
                    <li class="menu_links"></li>                                
                </ul>
            </a>
        </li>                 
    </ul>
</div>