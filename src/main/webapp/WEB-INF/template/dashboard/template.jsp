<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="pl">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Islands - MMORPG Game</title>

        <script src="${pageContext.request.contextPath}/resources/jquery-2.1.1.js"></script>
        <script src="${pageContext.request.contextPath}/resources/jquery-ui-1.9.2.js"></script>
        <script type='text/javascript' src='${pageContext.request.contextPath}/resources/knockout-3.2.0.js'></script>    

        <link href="${pageContext.request.contextPath}/resources/bootstrap-old/css/bootstrap.min.css" rel="stylesheet">

        <link href="${pageContext.request.contextPath}/resources/bootstrap-old/css/sb-admin.css" rel="stylesheet">

        <link href="${pageContext.request.contextPath}/resources/bootstrap-old/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link href="${pageContext.request.contextPath}/resources/css/mainstyle.css" rel="stylesheet" type="text/css">


        <link href="${pageContext.request.contextPath}/resources/css/jquery-ui.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src='${pageContext.request.contextPath}/resources/scripts/headerScripts.js'></script> 

        <script src="${pageContext.request.contextPath}/resources/bootstrap-old/js/bootstrap.min.js"></script>


        <tiles:importAttribute name="scripts" ignore="true"/>
        <c:forEach items="${scripts}" var="sc">
            <script src="${pageContext.request.contextPath}/resources/scripts/${sc}"></script>
        </c:forEach>

        <tiles:importAttribute name="styles" ignore="true"/>
        <c:forEach items="${styles}" var="s">
            <link  href="${pageContext.request.contextPath}/resources/css/${s}" rel="stylesheet"/>
        </c:forEach>

        <script>
            $(document).ready(function () {
                
                var model = new ModelView();
                model.update();
                ko.applyBindings(model, document.getElementById("page-wrapper"));
                
                
                var hmodel = new HeaderModelView(${headerModel.seconds}, model);
                ko.applyBindings(hmodel, document.getElementById("header"));
                hmodel.counter();                
            });

        </script>
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <tiles:insertAttribute name="header"/>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <tiles:insertAttribute name="menu"/>

                <!-- /.navbar-collapse -->
            </nav>

            <tiles:insertAttribute name="body"/>

            <tiles:insertAttribute name="footer"/>
        </div>





    </body>

</html>