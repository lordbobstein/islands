<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<div id="header">
    <div class="navbar-header">                
        <spring:url value="/" var="mainPageUrl" htmlEscape="true"/> 
        <a class="navbar-brand" href="${mainPageUrl}">Islands - MMORPG Game</a>
        <div class="navbar-brand">Tura: ${headerModel.stage}</div>
        <div class="navbar-brand">Czas pozostały do następnej tury: <span data-bind="text: stageTime().stageTime"></span> </div>
    </div>

    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
   
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <strong><shiro:principal/></strong> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <spring:url value="/auth/logout" var="logoutUrl" htmlEscape="true"/> 
                    <a href="${logoutUrl}"><i class="fa fa-fw fa-power-off"></i> Wylogowanie</a>                            
                </li>
            </ul>
        </li>
    </ul>
</div>

<script>
    

</script>
