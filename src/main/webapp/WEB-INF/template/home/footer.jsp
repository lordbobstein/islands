<!-- Footer -->
    <footer class="text-center">        
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright Piotr Bobek&copy; Islands 2014-2015
                    </div>
                </div>
            </div>
        </div>
    </footer>