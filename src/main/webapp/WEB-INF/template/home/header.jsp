<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<div class="header">  
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <spring:url value="/home" var="homeUrl" htmlEscape="true"/>                        
                <a href="${homeUrl}" class="navbar-brand">Strona główna</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <shiro:guest>
                        <li>
                            <spring:url value="/auth/login" var="loginUrl" htmlEscape="true"/> 
                            <a href="${loginUrl}">Logowanie</a>
                        </li>

                        <li>
                            <spring:url value="/auth/register" var="registerUrl" htmlEscape="true"/> 
                            <a href="${registerUrl}">Rejestracja</a>
                        </li>
                    </shiro:guest>
                    <shiro:authenticated>
                        <li>
                            <spring:url value="/dashboard/index" var="dashboardUrl" htmlEscape="true"/> 
                            <a href="${dashboardUrl}">Dashboard</a>
                        </li>
                        <li>
                            <spring:url value="/auth/logout" var="logoutUrl" htmlEscape="true"/> 
                            <a href="${logoutUrl}">Wylogowanie</a>
                        </li>                        
                    </shiro:authenticated>  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</div> 