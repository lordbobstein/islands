function Player(data)
{
    var self = this;

    self.id = ko.observable(data.id);
    self.login = ko.observable(data.login);
    self.numberOfIslands = ko.observable(data.numberOfIslands);

}

function ModelView()
{

    var self = this;

    self.playerList = ko.observableArray();

    self.update = function ()
    {
        $.post("/island/dashboard/results/getResults", function (data)
        {
            var players = $.map(data.resultsList, function (item) {
                return new Player(item);
            });

            self.playerList(players);
        });
    };


}