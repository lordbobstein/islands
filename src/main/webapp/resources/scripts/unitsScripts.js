$(function () {
    $(".tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
    $(".tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
});
$(function () {
    $("#accordion").accordion({
        heightStyle: "content",
        collapsible: false
    });
});

function ResourcesInfo(data)
{
    var self = this;

    self.foodValue = ko.observable(data.foodValue);
    self.stoneValue = ko.observable(data.stoneValue);
    self.woodValue = ko.observable(data.woodValue);


}

function ShipType(types)
{
    var self = this;

    self.type = ko.observable(types.type);
    self.enoughResourcesToBuild = ko.observable(types.enoughResourcesToBuild);
    self.foodCost = ko.observable(types.foodCost);
    self.woodCost = ko.observable(types.woodCost);
    self.stoneCost = ko.observable(types.stoneCost);
}

var warshipsTabCounter = 1;
var tradersTabCounter = 1;

function Trader(ship)
{
    var self = this;

    self.tabId = 'tradersTab' + tradersTabCounter;
    self.href = '#tradersTab' + tradersTabCounter;
    self.tabName = tradersTabCounter;
    tradersTabCounter += 1;

    self.id = ko.observable(ship.id);
    self.inMove = ko.observable(ship.inMove);
    self.requiredStages = ko.observable(ship.requiredStages);
    self.resourceType = ko.observable(ship.resourceType);

    self.currentLoc = ko.observable(ship.currentLocation);
    self.destinationLoc = ko.observable(ship.destinationLocation);

    self.currentLocation = ko.computed(function () {
        return "X: " + self.currentLoc().x + "   Y: " + self.currentLoc().y;
    }, this);

    self.destinationLocation = ko.computed(function () {
        return "X: " + self.destinationLoc().x + "   Y: " + self.destinationLoc().y;
    }, this);

    self.hold = ko.computed(function () {
        return ship.fillingHold + " /   " + ship.holdSize;
    }, this);

    self.possibletargets = ko.observableArray([]);
    self.targets = ko.observable(false);
    self.selected = ko.observable();
    self.isInThePort = ko.observable(ship.isInThePort);
}

function Warship(ship)
{
    var self = this;

    self.tabId = 'warshipsTab' + warshipsTabCounter;
    self.href = '#warshipsTab' + warshipsTabCounter;
    self.tabName = warshipsTabCounter;
    warshipsTabCounter += 1;

    self.id = ko.observable(ship.id);
    self.inMove = ko.observable(ship.inMove);
    self.requiredStages = ko.observable(ship.requiredStages);
    self.resourceType = ko.observable(ship.resourceType);

    self.currentLoc = ko.observable(ship.currentLocation);
    self.destinationLoc = ko.observable(ship.destinationLocation);

    self.currentLocation = ko.computed(function () {
        return "X: " + self.currentLoc().x + "   Y: " + self.currentLoc().y;
    }, this);

    self.destinationLocation = ko.computed(function () {
        return "X: " + self.destinationLoc().x + "   Y: " + self.destinationLoc().y;
    }, this);

    self.possibletargets = ko.observableArray([]);
    self.targets = ko.observable(false);
    self.selected = ko.observable();
    self.isInThePort = ko.observable(ship.isInThePort);
}

function ModelView()
{
    var self = this;

    self.shipTypes = ko.observableArray([]);
    self.traders = ko.observableArray([]);
    self.warships = ko.observableArray([]);
    self.warshipsReady = ko.observable(false);
    self.tradersReady = ko.observable(false);
    self.resourcesInfo = ko.observable();

    self.updateResources = function (shipType)
    {
        var foodCost = shipType.foodCost();
        var woodCost = shipType.woodCost();
        var stoneCost = shipType.stoneCost();

        var foodValue = self.resourcesInfo().foodValue();
        self.resourcesInfo().foodValue(foodValue - foodCost);

        var stoneValue = self.resourcesInfo().stoneValue();
        self.resourcesInfo().stoneValue(stoneValue - stoneCost);

        var woodValue = self.resourcesInfo().woodValue();
        self.resourcesInfo().woodValue(woodValue - woodCost);
    };

    self.updateShipTypes = function ()
    {
        for (i = 0; i < self.shipTypes().length; i++)
        {
            var foodCost = self.shipTypes()[i].foodCost();
            var woodCost = self.shipTypes()[i].woodCost();
            var stoneCost = self.shipTypes()[i].stoneCost();

            var foodValue = self.resourcesInfo().foodValue();
            var stoneValue = self.resourcesInfo().stoneValue();
            var woodValue = self.resourcesInfo().woodValue();

            if (foodCost > foodValue || woodCost > woodValue || stoneCost > stoneValue)
                self.shipTypes()[i].enoughResourcesToBuild(false);
            else
                self.shipTypes()[i].enoughResourcesToBuild(true);
        }
    };

    self.buildUnit = function (shipType)
    {
        self.updateResources(shipType);
        self.updateShipTypes();

        $.post("/island/dashboard/units/build", {unitType: shipType.type}, function (data)
        {
            var types = $.map(data, function (item) {
                return new ShipType(item);
            });

            self.shipTypes(types);
        });
    };

    self.return = function (ship)
    {
        $.post("/island/dashboard/units/return", {unitId: ship.id}, function (data)
        {
            ship.inMove(true);
            ship.isInThePort(false);
            ship.destinationLoc(data.destinationLocation);
            ship.requiredStages(data.stages);
        });
    };

    self.getPossibleTargets = function (ship)
    {
        if (ship.possibletargets().length === 0)
        {
            $.post("/island/dashboard/units/possibletargets", {unitId: ship.id}, function (data)
            {
                ship.possibletargets(data);
                ship.targets(true);

            });
        }
    };

    self.newCourse = function (ship)
    {
        $.post("/island/dashboard/units/newcourse", {unitId: ship.id, islandId: ship.selected().id}, function (data)
        {
            ship.inMove(true);
            ship.isInThePort(false);
            ship.destinationLoc(data.destinationLocation);
            ship.requiredStages(ship.selected().requiredStages);
            ship.resourceType(ship.selected().islandResourcesType);
        });
    };

    self.update = function ()
    {
        if (self.warshipsReady() === false)
        {
            $.post("/island/dashboard/units/getUnits", {needResourcesInfo: true}, function (data)
            {
                warshipsTabCounter = 1;
                tradersTabCounter = 1;

                var types = $.map(data.types, function (item) {
                    return new ShipType(item);
                });
                var traders = $.map(data.traders, function (item) {
                    return new Trader(item);
                });
                var warships = $.map(data.warships, function (item) {

                    return new Warship(item);
                });

                self.resourcesInfo(new ResourcesInfo(data.resourcesInfo));
                self.shipTypes(types);
                self.traders(traders);
                self.warships(warships);
                self.warshipsReady(true);
                self.tradersReady(true);
            });
        }
        else
        {
            $.post("/island/dashboard/units/getUnits", {needResourcesInfo: false}, function (data)
            {
                warshipsTabCounter = 1;
                tradersTabCounter = 1;

                var types = $.map(data.types, function (item) {
                    return new ShipType(item);
                });
                var traders = $.map(data.traders, function (item) {
                    return new Trader(item);
                });
                var warships = $.map(data.warships, function (item) {

                    return new Warship(item);
                });

                self.shipTypes(types);
                self.traders(traders);
                self.warships(warships);
                self.warshipsReady(true);
                self.tradersReady(true);
            });
        }


    };
}