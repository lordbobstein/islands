function StatisticModel(data)
{
    var self = this;

    self.foodValue = ko.observable(data.foodValue);
    self.stoneValue = ko.observable(data.stoneValue);
    self.woodValue = ko.observable(data.woodValue);
    self.isBuildingInQueue = ko.observable(data.isBuildingInQueue);
    self.numberOfBuildings = ko.observable(data.numberOfBuildings);
    self.numberOfUnits = ko.observable(data.numberOfUnits);
    self.numberOfIslands = ko.observable(data.numberOfIslands);
    self.taskList = ko.observableArray(data.taskList);
}

function ModelView()
{
    var self = this;

    self.statistics = ko.observable();

    self.update = function ()
    {
        $.post("/island/dashboard/getData", function (data)
        {
            var stats = new StatisticModel(data);
            self.statistics(stats);
        });
    };
}