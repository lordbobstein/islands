$(function () {
    $("#tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
    $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
});

function Building(data)
{
    var self = this;
    
    self.buildingType = ko.observable(data.buildingType);
    self.level = ko.observable(data.level);
    self.stoneCost = ko.observable(data.stoneCost);
    self.woodCost = ko.observable(data.woodCost);
    self.foodCost = ko.observable(data.foodCost);
    self.enoughResourcesToBuild = ko.observable(data.enoughResourcesToBuild);
    self.built = ko.observable(data.built);
}

function ModelView()
{
    var self = this;

    self.buildings = ko.observableArray();
    self.wasBuilt = ko.observable();

    self.buildBuilding = function (item)
    {
        $.post("/island/dashboard/buildings/buildbuilding", {buildingType: item.buildingType()});
        self.wasBuilt(true);
    };

    self.update = function () {
        $.post("/island/dashboard/buildings/getBuildings", function (data)
        {
            var built = $.map(data.buildings, function (item) {
                return new Building(item);
            });

            self.buildings(built);
            self.wasBuilt(data.wasBuilt);
        });
    };
}


