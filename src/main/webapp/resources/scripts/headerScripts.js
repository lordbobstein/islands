function StageTime(sec)
{
    var self = this;

    self.seconds = ko.observable(sec);
    self.stageTime = ko.computed(function ()
    {
        var min = self.seconds() / 60;

        if (min < 1)
            min = 0;
        var sec = self.seconds() - (min * 60);

        return min + ":" + sec;
    });
}

function HeaderModelView(seconds, model)
{
    var self = this;

    self.stageTime = ko.observable(new StageTime(seconds));
            self.counter = function ()
            {
                setInterval(self.timer, 1000);
            };

    self.timer = function ()
    {
        self.stageTime().seconds(self.stageTime().seconds() - 1);
        if (self.stageTime().seconds() <= 0)
        {
            setTimeout(model.update, 5000);
            self.stageTime().seconds(60);
        }
    };
}