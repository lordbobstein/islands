/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

import com.lordbobstein.island.model.entities.unit.Trader;
import com.lordbobstein.island.model.entities.unit.Unit;
import com.lordbobstein.island.model.entities.unit.Warship;
import com.lordbobstein.island.model.logic.common.enums.UnitType;

/**
 *
 * @author lordbobstein
 */
public class UnitsTypeViewModel
{
    private UnitType type;
    private boolean enoughResourcesToBuild;
    private int foodCost;
    private int woodCost;
    private int stoneCost;

    public UnitsTypeViewModel(Unit unit)
    {
        if (unit instanceof Trader)
        {
            this.type = UnitType.TRADER;
        }
        else if (unit instanceof Warship)
        {
            this.type = UnitType.WARSHIP;
        }
        
        this.enoughResourcesToBuild = unit.isEnoughResourcesToBuild();
        this.foodCost = unit.getCostFood();
        this.woodCost = unit.getCostWood();
        this.stoneCost = unit.getCostStone();
    }

    public UnitType getType()
    {
        return type;
    }

    public void setType(UnitType type)
    {
        this.type = type;
    }

    public boolean isEnoughResourcesToBuild()
    {
        return enoughResourcesToBuild;
    }

    public void setEnoughResourcesToBuild(boolean enoughResourcesToBuild)
    {
        this.enoughResourcesToBuild = enoughResourcesToBuild;
    }

    public int getFoodCost()
    {
        return foodCost;
    }

    public void setFoodCost(int foodCost)
    {
        this.foodCost = foodCost;
    }

    public int getWoodCost()
    {
        return woodCost;
    }

    public void setWoodCost(int woodCost)
    {
        this.woodCost = woodCost;
    }

    public int getStoneCost()
    {
        return stoneCost;
    }

    public void setStoneCost(int stoneCost)
    {
        this.stoneCost = stoneCost;
    }
    
    
    
    


    

    
    
    
    
}
