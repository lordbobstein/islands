/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

/**
 *
 * @author lordbobstein
 */
public class PlayerResourcesViewModel
{
    private int foodValue;
    private int woodValue;
    private int stoneValue;

    public PlayerResourcesViewModel(int foodValue, int woodValue, int stoneValue)
    {
        this.foodValue = foodValue;
        this.woodValue = woodValue;
        this.stoneValue = stoneValue;
    }   

    public int getFoodValue()
    {
        return foodValue;
    }

    public void setFoodValue(int foodValue)
    {
        this.foodValue = foodValue;
    }

    public int getWoodValue()
    {
        return woodValue;
    }

    public void setWoodValue(int woodValue)
    {
        this.woodValue = woodValue;
    }

    public int getStoneValue()
    {
        return stoneValue;
    }

    public void setStoneValue(int stoneValue)
    {
        this.stoneValue = stoneValue;
    }
    
    
}
