/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

/**
 *
 * @author lordbobstein
 */
public class ResultPlayerModelView
{
    private int id;
    private String login;
    private int numberOfIslands;

    public ResultPlayerModelView(int id, String login, int numberOfIslands)
    {
        this.id = id;
        this.login = login;
        this.numberOfIslands = numberOfIslands;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public int getNumberOfIslands()
    {
        return numberOfIslands;
    }

    public void setNumberOfIslands(int numberOfIslands)
    {
        this.numberOfIslands = numberOfIslands;
    }
    
    
    
    
}
