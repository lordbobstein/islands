/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.unit.Trader;
import com.lordbobstein.island.model.entities.unit.Unit;

/**
 *
 * @author lordbobstein
 */
public class UnitViewModel
{

    private Integer id;

    private Location startLocation;

    private Location currentLocation;

    private Location destinationLocation;

    private boolean inMove;

    private int requiredStages;

    private int speed;

    private String resourceType;

    private boolean enoughResourcesToBuild;

    private int holdSize;

    private int fillingHold;

    private boolean isInThePort;

    public UnitViewModel(Unit unit, boolean isInThePort)
    {
        this.id = unit.getId();        
        this.currentLocation = unit.getCurrentLocation();
        this.destinationLocation = unit.getDestinationLocation();
        this.inMove = unit.isInMove();
        this.requiredStages = unit.getRequiredStages();
        this.speed = unit.getSpeed();       
        this.enoughResourcesToBuild = unit.isEnoughResourcesToBuild();
        this.isInThePort = isInThePort;

        if (unit.getResourceType() != null)
        {
            switch (unit.getResourceType())
            {
                case FOOD:
                    resourceType = "POŻYWIENIE";
                    break;

                case STONE:
                    resourceType = "KAMIEŃ";
                    break;

                case WOOD:
                    resourceType = "DREWNO";
                    break;
            }
        }
        else
        {
            resourceType = "";
        }

        if (unit instanceof Trader)
        {
            Trader t = (Trader) unit;
            this.holdSize = t.getHoldSize();
            this.fillingHold = t.getFillingHold();
        }
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Location getStartLocation()
    {
        return startLocation;
    }

    public void setStartLocation(Location startLocation)
    {
        this.startLocation = startLocation;
    }

    public Location getCurrentLocation()
    {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation)
    {
        this.currentLocation = currentLocation;
    }

    public Location getDestinationLocation()
    {
        return destinationLocation;
    }

    public void setDestinationLocation(Location destinationLocation)
    {
        this.destinationLocation = destinationLocation;
    }

    public boolean isInMove()
    {
        return inMove;
    }

    public void setInMove(boolean inMove)
    {
        this.inMove = inMove;
    }

    public int getRequiredStages()
    {
        return requiredStages;
    }

    public void setRequiredStages(int requiredStages)
    {
        this.requiredStages = requiredStages;
    }

    public int getSpeed()
    {
        return speed;
    }

    public void setSpeed(int speed)
    {
        this.speed = speed;
    }

    public String getResourceType()
    {
        return resourceType;
    }

    public void setResourceType(String resourceType)
    {
        this.resourceType = resourceType;
    }

    public boolean isEnoughResourcesToBuild()
    {
        return enoughResourcesToBuild;
    }

    public void setEnoughResourcesToBuild(boolean enoughResourcesToBuild)
    {
        this.enoughResourcesToBuild = enoughResourcesToBuild;
    }

    public int getHoldSize()
    {
        return holdSize;
    }

    public void setHoldSize(int holdSize)
    {
        this.holdSize = holdSize;
    }

    public int getFillingHold()
    {
        return fillingHold;
    }

    public void setFillingHold(int fillingHold)
    {
        this.fillingHold = fillingHold;
    }

    public boolean isIsInThePort()
    {
        return isInThePort;
    }

    public void setIsInThePort(boolean isInThePort)
    {
        this.isInThePort = isInThePort;
    }

}
