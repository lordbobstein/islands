/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

import com.lordbobstein.island.model.logic.factories.buildings.BuildingDomain;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lordbobstein
 */
public class BuildingsViewModel
{
    private List<BuildingViewModel> buildings;    
    private boolean wasBuilt;

    public BuildingsViewModel(List<BuildingDomain> builtBuildings, boolean wasBuilt)
    {
        this.buildings = new ArrayList<>();      
        
        for (BuildingDomain buildingDomain : builtBuildings)
        {            
            this.buildings.add(new BuildingViewModel(buildingDomain));    
        }
        
        this.wasBuilt = wasBuilt;
    }

    public List<BuildingViewModel> getBuildings()
    {
        return buildings;
    }

    public void setBuildings(List<BuildingViewModel> buildings)
    {
        this.buildings = buildings;
    }

    public boolean isWasBuilt()
    {
        return wasBuilt;
    }

    public void setWasBuilt(boolean wasBuilt)
    {
        this.wasBuilt = wasBuilt;
    }

}
