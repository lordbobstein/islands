/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

/**
 *
 * @author lordbobstein
 */
public class DashboardViewModel
{
    private int version;
    
    private int foodValue;
    private int stoneValue;
    private int woodValue;
    
    private int numberOfBuildings;
    private boolean isBuildingInQueue;
    
    private int numberOfUnits;
    
    private int numberOfIslands; 

    public DashboardViewModel(int version, int foodValue, int stoneValue, int woodValue, int numberOfBuildings, boolean isBuildingInQueue, int numberOfUnits, int numberOfIslands)
    {
        this.version = version;
        this.foodValue = foodValue;
        this.stoneValue = stoneValue;
        this.woodValue = woodValue;
        this.numberOfBuildings = numberOfBuildings;
        this.isBuildingInQueue = isBuildingInQueue;
        this.numberOfUnits = numberOfUnits;
        this.numberOfIslands = numberOfIslands;        
    }

    public int getVersion()
    {
        return version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }
    
    public int getFoodValue()
    {
        return foodValue;
    }

    public void setFoodValue(int foodValue)
    {
        this.foodValue = foodValue;
    }

    public int getStoneValue()
    {
        return stoneValue;
    }

    public void setStoneValue(int stoneValue)
    {
        this.stoneValue = stoneValue;
    }

    public int getWoodValue()
    {
        return woodValue;
    }

    public void setWoodValue(int woodValue)
    {
        this.woodValue = woodValue;
    }

    public int getNumberOfBuildings()
    {
        return numberOfBuildings;
    }

    public void setNumberOfBuildings(int numberOfBuildings)
    {
        this.numberOfBuildings = numberOfBuildings;
    }

    public boolean isIsBuildingInQueue()
    {
        return isBuildingInQueue;
    }

    public void setIsBuildingInQueue(boolean isBuildingInQueue)
    {
        this.isBuildingInQueue = isBuildingInQueue;
    }

    public int getNumberOfUnits()
    {
        return numberOfUnits;
    }

    public void setNumberOfUnits(int numberOfUnits)
    {
        this.numberOfUnits = numberOfUnits;
    }

    public int getNumberOfIslands()
    {
        return numberOfIslands;
    }

    public void setNumberOfIslands(int numberOfIslands)
    {
        this.numberOfIslands = numberOfIslands;
    }
}
