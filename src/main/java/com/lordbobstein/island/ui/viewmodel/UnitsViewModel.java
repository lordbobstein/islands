/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;


import java.util.List;

/**
 *
 * @author lordbobstein
 */
public class UnitsViewModel
{
    private List<UnitViewModel> traders;
    private List<UnitViewModel> warships;
    private List<UnitsTypeViewModel> types;
    private PlayerResourcesViewModel resourcesInfo;

    public UnitsViewModel(List<UnitViewModel> traders, List<UnitViewModel> warships, List<UnitsTypeViewModel> types)
    {
        this.traders = traders;        
        this.warships = warships;
        this.types = types;
    }

    public UnitsViewModel(List<UnitViewModel> traders, List<UnitViewModel> warships, List<UnitsTypeViewModel> types, PlayerResourcesViewModel resourcesInfo)
    {
        this.traders = traders;
        this.warships = warships;
        this.types = types;
        this.resourcesInfo = resourcesInfo;
    }
    

    public PlayerResourcesViewModel getResourcesInfo()
    {
        return resourcesInfo;
    }

    public void setResourcesInfo(PlayerResourcesViewModel resourcesInfo)
    {
        this.resourcesInfo = resourcesInfo;
    }
    
    

    public List<UnitViewModel> getTraders()
    {
        return traders;
    }

    public void setTraders(List<UnitViewModel> traders)
    {
        this.traders = traders;
    }

    public List<UnitViewModel> getWarships()
    {
        return warships;
    }

    public void setWarships(List<UnitViewModel> warships)
    {
        this.warships = warships;
    }

    public List<UnitsTypeViewModel> getTypes()
    {
        return types;
    }

    public void setTypes(List<UnitsTypeViewModel> types)
    {
        this.types = types;
    }
}
