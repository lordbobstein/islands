/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.logic.common.enums.IslandResourcesType;

/**
 *
 * @author lordbobstein
 */
public class NewCourseViewModel
{

    private Location destinationLocation;
    private int stages;
    private String resourceType;

    public NewCourseViewModel(Location destinationLocation, int stages, IslandResourcesType resourcesType)
    {
        this.destinationLocation = destinationLocation;
        this.stages = stages;
        switch (resourcesType)
            {
                case FOOD:
                    resourceType = "POŻYWIENIE";
                    break;

                case STONE:
                    resourceType = "KAMIEŃ";
                    break;

                case WOOD:
                    resourceType = "DREWNO";
                    break;
            }
        
    }

    public NewCourseViewModel(Location destinationLocation, int stages)
    {
        this.destinationLocation = destinationLocation;
        this.stages = stages;
    }

    public Location getDestinationLocation()
    {
        return destinationLocation;
    }

    public void setDestinationLocation(Location destinationLocation)
    {
        this.destinationLocation = destinationLocation;
    }

    public int getStages()
    {
        return stages;
    }

    public void setStages(int stages)
    {
        this.stages = stages;
    }

    public String getResourceType()
    {
        return resourceType;
    }

    

    

}
