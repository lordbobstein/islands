/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

/**
 *
 * @author lordbobstein
 */
public class HeaderViewModel
{
    private int stage;
    private int seconds;

    public HeaderViewModel(int stage, int seconds)
    {
        this.stage = stage;
        this.seconds = seconds;
    }

    public int getStage()
    {
        return stage;
    }

    public void setStage(int stage)
    {
        this.stage = stage;
    }

    public int getSeconds()
    {
        return seconds;
    }

    public void setSeconds(int seconds)
    {
        this.seconds = seconds;
    }
}
