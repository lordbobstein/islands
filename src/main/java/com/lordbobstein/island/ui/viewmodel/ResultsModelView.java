/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

import com.lordbobstein.island.model.entities.player.Player;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lordbobstein
 */
public class ResultsModelView
{
    private List<ResultPlayerModelView> resultsList;

    public ResultsModelView(List<Player> playerList)
    {
        resultsList = new ArrayList<>();
        
        for(Player player : playerList)
        {
            int id = player.getId();
            String login = player.getPlayerData().getLogin();
            int numberOfIslands = player.getIslands().size();
            
            resultsList.add(new ResultPlayerModelView(id, login, numberOfIslands));
        }
    }

    public List<ResultPlayerModelView> getResultsList()
    {
        return resultsList;
    }

    public void setResultsList(List<ResultPlayerModelView> resultsList)
    {
        this.resultsList = resultsList;
    }
    
    
    
}
