/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

import com.lordbobstein.island.model.logic.common.enums.IslandResourcesType;

/**
 *
 * @author lordbobstein
 */
public class UnitsSelectCourseViewModel implements Comparable<UnitsSelectCourseViewModel>
{

    private String islandResourcesType;
    private int requiredStages;
    private int id;

    public UnitsSelectCourseViewModel(IslandResourcesType islandResourcesType, int requiredStages, int id)
    {
        this.requiredStages = requiredStages;
        this.id = id;

        switch (islandResourcesType)
        {
            case FOOD:
                this.islandResourcesType = "POŻYWIENIE";
                break;

            case STONE:
                this.islandResourcesType = "KAMIEŃ";
                break;

            case WOOD:
                this.islandResourcesType = "DREWNO";
                break;
        }
    }

    @Override
    public int compareTo(UnitsSelectCourseViewModel o)
    {
        return this.requiredStages - o.getRequiredStages();
    }

    public String getIslandResourcesType()
    {
        return islandResourcesType;
    }

    public void setIslandResourcesType(String islandResourcesType)
    {
        this.islandResourcesType = islandResourcesType;
    }

    public int getRequiredStages()
    {
        return requiredStages;
    }

    public void setRequiredStages(int requiredStages)
    {
        this.requiredStages = requiredStages;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

}
