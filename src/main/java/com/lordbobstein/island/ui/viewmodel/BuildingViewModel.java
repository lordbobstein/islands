/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.logic.factories.buildings.BuildingDomain;

/**
 *
 * @author lordbobstein
 */
public class BuildingViewModel
{
    private int level;
    private int stoneCost;
    private int woodCost;
    private int foodCost;
    private boolean enoughResourcesToBuild;
    private boolean built;
    private BuildingType buildingType;

    public BuildingViewModel(BuildingType buildingType, int level, int stoneCost, int woodCost, int foodCost, boolean enoughResourcesToBuild, boolean built)
    {
        this.buildingType = buildingType;
        this.level = level;
        this.stoneCost = stoneCost;
        this.woodCost = woodCost;
        this.foodCost = foodCost;
        this.enoughResourcesToBuild = enoughResourcesToBuild;
        this.built = built;
    }

    public BuildingViewModel(BuildingDomain buildingDomain)
    {
        this.buildingType = buildingDomain.getBuildingType();
        this.level = buildingDomain.getLevel();
        this.stoneCost = buildingDomain.getCostStone();
        this.woodCost = buildingDomain.getCostWood();
        this.foodCost = buildingDomain.getCostFood();
        this.enoughResourcesToBuild = buildingDomain.isEnoughResourcesToBuild();
        this.built = buildingDomain.isBuilt();
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }

    public int getStoneCost()
    {
        return stoneCost;
    }

    public void setStoneCost(int stoneCost)
    {
        this.stoneCost = stoneCost;
    }

    public int getWoodCost()
    {
        return woodCost;
    }

    public void setWoodCost(int woodCost)
    {
        this.woodCost = woodCost;
    }

    public int getFoodCost()
    {
        return foodCost;
    }

    public void setFoodCost(int foodCost)
    {
        this.foodCost = foodCost;
    }

    public boolean isEnoughResourcesToBuild()
    {
        return enoughResourcesToBuild;
    }

    public void setEnoughResourcesToBuild(boolean enoughResourcesToBuild)
    {
        this.enoughResourcesToBuild = enoughResourcesToBuild;
    }

    public boolean isBuilt()
    {
        return built;
    }

    public void setBuilt(boolean built)
    {
        this.built = built;
    }

    public BuildingType getBuildingType()
    {
        return buildingType;
    }

    public void setBuildingType(BuildingType buildingType)
    {
        this.buildingType = buildingType;
    }
}
