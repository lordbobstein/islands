/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.viewmodel;

/**
 *
 * @author lordbobstein
 */
public class ResourceDetailsViewModel
{
    private int value;
    private int increase;
    private String name;

    public ResourceDetailsViewModel(int value, int increase, String name)
    {
        this.value = value;
        this.increase = increase;
        this.name = name;
    }

    public int getValue()
    {
        return value;
    }

    public int getIncrease()
    {
        return increase;
    }

    public String getName()
    {
        return name;
    }
    
    
    
    
    
}
