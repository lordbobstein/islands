/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.controller;

import com.lordbobstein.island.model.entities.player.PlayerData;
import com.lordbobstein.island.model.services.islandServices.IslandServices;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author lordbobstein
 */
@Controller
@RequestMapping("/auth")
public class AuthController
{
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
    
    @Autowired
    private ServletContext servletContext;
    
    @Autowired
    private IslandServices islandServices;
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login()
    {
        return "home/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public View doLogin(String username, String password, RedirectAttributes model)
    {
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        Subject currentUser = SecurityUtils.getSubject();
        try
        {
            currentUser.login(token);
        }
        catch (AuthenticationException ae)
        {
            logger.info(ae.getMessage()); 
            model.addFlashAttribute("error", "Wystąpił błąd logowania. Spróbuj jeszcze raz.");
            return new RedirectView("login");
        }
        
        if(currentUser.isAuthenticated())
        {
            if(username.equals("admin"))
            {
                return new RedirectView(servletContext.getContextPath() + "/admin/");
            }
            else
            {
                return new RedirectView(servletContext.getContextPath() + "/dashboard/");
            }
        
        }
        else
        {
            return new RedirectView(servletContext.getContextPath() + "/auth/login/");
        }
        
        
    }
    
    @RequestMapping(value = "/register")
    public String register(Model model)
    {
        model.addAttribute("playerData",new PlayerData());
        return "home/register";
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String doRegister(@Valid PlayerData playerData, BindingResult bindingResult, Model model)
    {
        
        if (bindingResult.hasErrors())
        {  
            return "home/register";
        }
        String username = playerData.getLogin();
        String password = playerData.getPassword();
        
        boolean done = islandServices.playerService.createPlayer(username, password);
        
        if (done)
        {
            Subject currentUser = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(username, password);           
            token.setRememberMe(true);
            currentUser.login(token);            
            
            return "redirect:/dashboard/";
        }
        else
        {     
            model.addAttribute("error", "Wystąpił błąd rejestracji. Spróbuj jeszcze raz. Spróbuj inny login.");
            return "home/register";
        }
    }
}
