/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.controller;

import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.entities.resource.Resource;
import com.lordbobstein.island.model.services.islandServices.IslandServices;
import com.lordbobstein.island.ui.viewmodel.DashboardViewModel;
import com.lordbobstein.island.ui.viewmodel.HeaderViewModel;
import com.lordbobstein.island.ui.viewmodel.ResourceDetailsViewModel;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author lordbobstein
 */
@Controller
@RequestMapping(value = "/dashboard")
public class DashboardController
{

    protected Logger logger = Logger.getLogger(getClass());

    @Autowired
    private IslandServices services;

    private DashboardViewModel dashboardViewModel;

    @RequestMapping(value =
    {
        "/index", "/"
    }, method = RequestMethod.GET)
    public ModelAndView index()
    {
        ModelAndView model = new ModelAndView("dashboard/index");
        model.addObject(
                "headerModel", getHeaderViewModel());

        return model;
    }

    @RequestMapping(value = "/getData", method = RequestMethod.POST)
    @ResponseBody  
    public DashboardViewModel getData()
    {
        int stage = services.archipelagService.getCurrentStage();
//        int modelVersion = 0;
//        
//        if(dashboardViewModel != null)
//        {
//            modelVersion = dashboardViewModel.getVersion();
//        }
//
//        if ((dashboardViewModel == null) || (modelVersion < stage))
//        {
            String login = getPlayerLogin();

            Player player = services.playerService.getPlayerByLogin(login);

            int foodValue = player.getFood().getResourceValue();
            int stoneValue = player.getStone().getResourceValue();
            int woodValue = player.getWood().getResourceValue();
            int numberOfBuildings = player.getBuildings().size();
            boolean isBuildingInQueue = player.isBuildingInQueue();
            int numberOfUnits = player.getUnits().size();
            int numberOfIslands = services.playerService.countPlayerIsland(login) + 1;

            DashboardViewModel viewModel
                    = new DashboardViewModel(stage, foodValue, stoneValue,
                            woodValue, numberOfBuildings, isBuildingInQueue,
                            numberOfUnits, numberOfIslands);
            
            viewModel.setVersion(stage);
            dashboardViewModel = viewModel;
            
            return viewModel;
//        }
//        else
//        {
//            return dashboardViewModel;
//        }
        
    }

    @RequestMapping(value = "/resourcedetails", method = RequestMethod.GET)
    public ModelAndView resourceDetails(@RequestParam("target") String target)
    {
        String login = getPlayerLogin();

        Resource resource = null;
        String resourceName = null;
        Player player = services.playerService.getPlayerByLogin(login);

        switch (target)
        {
            case "food":
                resource = player.getFood();
                resourceName = "Jedzenie";
                break;

            case "stone":
                resource = player.getStone();
                resourceName = "Kamień";
                break;

            case "wood":
                resource = player.getWood();
                resourceName = "Drzewo";
                break;

            default:
            //usupelnic o redirect do index
        }

        ResourceDetailsViewModel viewModel = new ResourceDetailsViewModel(resource.getResourceValue(),
                resource.getResourceIncrease(), resourceName);

        ModelAndView model = new ModelAndView("dashboard/resourcedetails");

        model.addObject("resource", viewModel);
        model.addObject("headerModel", getHeaderViewModel());

        return model;
    }

    private HeaderViewModel getHeaderViewModel()
    {
        int stage = services.archipelagService.getCurrentStage();
        int seconds = services.gameService.getNextRoundTime();
        return new HeaderViewModel(stage, seconds);
    }

    private String getPlayerLogin()
    {
        Subject subject = SecurityUtils.getSubject();
        return (String) subject.getPrincipal();
    }
}
