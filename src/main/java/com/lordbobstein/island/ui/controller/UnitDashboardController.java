/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.controller;

import com.lordbobstein.island.model.dao.islandsDAO.IslandDAO;
import com.lordbobstein.island.model.entities.island.Island;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.unit.Trader;
import com.lordbobstein.island.model.entities.unit.Unit;
import com.lordbobstein.island.model.entities.unit.Warship;
import com.lordbobstein.island.model.logic.common.enums.IslandResourcesType;
import com.lordbobstein.island.model.logic.common.enums.UnitType;
import com.lordbobstein.island.model.services.islandServices.IslandServices;
import com.lordbobstein.island.ui.viewmodel.HeaderViewModel;
import com.lordbobstein.island.ui.viewmodel.NewCourseViewModel;
import com.lordbobstein.island.ui.viewmodel.PlayerResourcesViewModel;
import com.lordbobstein.island.ui.viewmodel.UnitViewModel;
import com.lordbobstein.island.ui.viewmodel.UnitsSelectCourseViewModel;
import com.lordbobstein.island.ui.viewmodel.UnitsTypeViewModel;
import com.lordbobstein.island.ui.viewmodel.UnitsViewModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author lordbobstein
 */
@Controller
@RequestMapping(value = "/dashboard/units")
public class UnitDashboardController
{

    @Autowired
    private IslandServices services;

    @RequestMapping(value =
    {
        "/", ""
    })
    public ModelAndView unitsIndex()
    {
        ModelAndView model = new ModelAndView("dashboard/units");
        model.addObject(
                "headerModel", getHeaderViewModel());

        return model;
    }

    @RequestMapping(value = "/getUnits", method = RequestMethod.POST)
    @ResponseBody
    public UnitsViewModel getUnits(@RequestParam boolean needResourcesInfo)
    {

        String login = getPlayerLogin();
        Player player = services.playerService.getPlayerByLogin(login);
        List<UnitViewModel> traders = new ArrayList<>();
        List<UnitViewModel> warships = new ArrayList<>();
        boolean isInThePort;

        for (Unit unit : player.getUnits())
        {
            isInThePort = services.unitsService.checkIfTheUnitIsInThePort(unit);

            if (unit instanceof Trader)
            {
                unit.setRequiredStages(unit.getRequiredStages());
                traders.add(new UnitViewModel(unit, isInThePort));

            }
            else if (unit instanceof Warship)
            {
                unit.setRequiredStages(unit.getRequiredStages());
                warships.add(new UnitViewModel(unit, isInThePort));
            }

        }
        List<UnitsTypeViewModel> types = services.unitsService.prepareUnitsTypeList(login);
        UnitsViewModel viewModel = null;

        if (needResourcesInfo)
        {
            int foodValue = player.getFood().getResourceValue();
            int woodValue = player.getWood().getResourceValue();
            int stoneValue = player.getStone().getResourceValue();
            PlayerResourcesViewModel playerResourcesViewModel = new PlayerResourcesViewModel(foodValue, woodValue, stoneValue);

            viewModel = new UnitsViewModel(traders, warships, types, playerResourcesViewModel);
        }
        else
        {
            viewModel = new UnitsViewModel(traders, warships, types);
        }

        return viewModel;
    }

    @RequestMapping(value = "/build", method = RequestMethod.POST)
    @ResponseBody
    public List<UnitsTypeViewModel> buildUnit(@RequestParam(value = "unitType") UnitType unitType)
    {
        String login = getPlayerLogin();
        services.taskService.createBuildNewUnitTask(login, unitType);
        List<UnitsTypeViewModel> types = services.unitsService.prepareUnitsTypeList(login);

        return types;
    }

    @RequestMapping(value = "/newcourse", method = RequestMethod.POST, params =
    {
        "unitId", "islandId"
    })
    @ResponseBody
    public NewCourseViewModel newCourse(@RequestParam(value = "unitId") int unitId, @RequestParam(value = "islandId") int islandId)
    {
        Island island = services.archipelagService.getIslandById(islandId);
        Location islandLocation = island.getLocation();
        IslandResourcesType resourceType = island.getResourceType();
        int stages = services.unitsService.createNewCourseData(unitId, islandLocation, resourceType);

        NewCourseViewModel model = new NewCourseViewModel(islandLocation, stages, resourceType);

        return model;
    }

    @RequestMapping(value = "/return", method = RequestMethod.POST, params = "unitId")
    @ResponseBody
    public NewCourseViewModel returnToPort(@RequestParam(value = "unitId") int unitId)
    {
        Location location = services.unitsService.getLocationOfPort(unitId);
        int stages = services.unitsService.createNewCourseData(unitId, location);

        NewCourseViewModel model = new NewCourseViewModel(location, stages);

        return model;
    }

    @RequestMapping(value = "/possibletargets", method = RequestMethod.POST)
    @ResponseBody
    public List<UnitsSelectCourseViewModel> getPossibleCourses(@RequestParam(value = "unitId") int unitId)
    {
        List<UnitsSelectCourseViewModel> viewModel = new ArrayList<>();
        Unit unit = services.unitsService.getUnitById(unitId);

        int archipelagoId = services.unitsService.getArchipelagoId(unitId);
        List<Island> islandList = services.archipelagService.getResourcesIslandInArchipelago(archipelagoId);
        int stages;

        for (Island island : islandList)
        {
            unit.setDestinationLocation(island.getLocation());
            stages = unit.calculateRequiredStages();

            UnitsSelectCourseViewModel unitsSelectCourseViewModel
                    = new UnitsSelectCourseViewModel(island.getResourceType(), stages, island.getId());

            viewModel.add(unitsSelectCourseViewModel);
        }

        Collections.sort(viewModel);

        return viewModel;
    }

    private HeaderViewModel getHeaderViewModel()
    {
        int stage = services.archipelagService.getCurrentStage();
        int seconds = services.gameService.getNextRoundTime();
        return new HeaderViewModel(stage, seconds);
    }

    private String getPlayerLogin()
    {
        Subject subject = SecurityUtils.getSubject();
        return (String) subject.getPrincipal();
    }

}
