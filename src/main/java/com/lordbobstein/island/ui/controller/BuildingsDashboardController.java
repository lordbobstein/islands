/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.controller;

import com.lordbobstein.island.model.entities.building.Building;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.logic.common.costs.Costs;
import com.lordbobstein.island.model.logic.factories.buildings.BuildingDomain;
import com.lordbobstein.island.model.logic.factories.buildings.BuildingFactory;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.services.islandServices.IslandServices;
import com.lordbobstein.island.ui.viewmodel.BuildingsViewModel;
import com.lordbobstein.island.ui.viewmodel.HeaderViewModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author lordbobstein
 */
@Controller
@RequestMapping(value = "/dashboard/buildings")
public class BuildingsDashboardController
{

    @Autowired
    private IslandServices services;

    @RequestMapping(value =
    {
        "", "/"
    }, method = RequestMethod.GET)
    public ModelAndView index()
    {
        ModelAndView model = new ModelAndView("dashboard/buildings");
        model.addObject("headerModel", getHeaderViewModel());

        return model;
    }

    @RequestMapping(value = "getBuildings", method = RequestMethod.POST)
    @ResponseBody
    public BuildingsViewModel getBuildings()
    {
        BuildingsViewModel viewModel;
        BuildingFactory buildingFactory = new BuildingFactory();
        String login = getPlayerLogin();
        Player player = services.playerService.getPlayerByLogin(login);

        Map<BuildingType, Building> buildings = player.getBuildings();

        List<BuildingDomain> viewBuildings = new ArrayList<>();

        for (BuildingType typ : BuildingType.values())
        {
            BuildingDomain buildingDomain;
            Building building;

            if (buildings.containsKey(typ))
            {
                building = buildings.get(typ);
                buildingDomain = buildingFactory.create(typ, building.getPoziom());
                buildingDomain.getCosts().calculateCosts(buildingDomain.getLevel() + 1);
                buildingDomain.setBuilt(true);
            }
            else
            {
                //tworzy nowy budynek(z poziomem 0 - nie istnieje)
                buildingDomain = buildingFactory.create(typ, 0);
                buildingDomain.setBuilt(false);
            }

            if (services.buildingsService
                    .sprawdzMozliwoscWybudowaniaBudynku(typ, buildingDomain.getLevel(), login))
            {
                buildingDomain.setEnoughResourcesToBuild(true);
            }
            else
            {
                buildingDomain.setEnoughResourcesToBuild(false);
            }

            viewBuildings.add(buildingDomain);
        }

        boolean wasBuilt = player.isBuildingInQueue();
        viewModel = new BuildingsViewModel(viewBuildings, wasBuilt);

        return viewModel;
    }

    @RequestMapping(value = "/buildbuilding", method = RequestMethod.POST)
    @ResponseBody
    public String buildOrUpdateBuilding(@RequestParam(value = "buildingType") BuildingType buildingType)
    {
        String login = getPlayerLogin();

        services.taskService.createBuildOrUpgradeBuildingTask(login, buildingType);
        return "OK";
    }

    private HeaderViewModel getHeaderViewModel()
    {
        int stage = services.archipelagService.getCurrentStage();
        int seconds = services.gameService.getNextRoundTime();
        return new HeaderViewModel(stage, seconds);
    }

    private String getPlayerLogin()
    {
        Subject subject = SecurityUtils.getSubject();
        return (String) subject.getPrincipal();
    }

}
