/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.controller;

import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.services.islandServices.IslandServices;
import com.lordbobstein.island.ui.viewmodel.HeaderViewModel;
import com.lordbobstein.island.ui.viewmodel.ResultsModelView;
import java.util.List;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author lordbobstein
 */
@Controller
@RequestMapping(value = "/dashboard/results")
public class ResultsController
{
    @Autowired
    private IslandServices services;
    
    @RequestMapping(value = { "/", "" })
    public ModelAndView index()
    {
        ModelAndView model = new ModelAndView("dashboard/results");
        model.addObject(
                "headerModel", getHeaderViewModel());
        
        return model;
    }
    
    @RequestMapping(value = "/getResults", method = RequestMethod.POST)
    @ResponseBody
    public ResultsModelView getResults()
    {
        String login = getPlayerLogin();
        int archipelagoId = services.playerService.getPlayerByLogin(login).getArchipelago().getId();
        List<Player> playerList = services.playerService.
                findAndSortPlayersByNumberOfIsland(archipelagoId);
        
        ResultsModelView model = new ResultsModelView(playerList);
        
        return model;        
    }
    
    
    private HeaderViewModel getHeaderViewModel()
    {
        int stage = services.archipelagService.getCurrentStage();
        int seconds = services.gameService.getNextRoundTime();
        return new HeaderViewModel(stage, seconds);
    }
    
    private String getPlayerLogin()
    {
        Subject subject = SecurityUtils.getSubject();
        return (String) subject.getPrincipal();
    }
}
