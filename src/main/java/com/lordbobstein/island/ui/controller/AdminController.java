/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.ui.controller;

import com.lordbobstein.island.model.dao.archipelagosDAO.ArchipelagosDAO;
import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.services.islandServices.IslandServices;
import java.util.Date;
import java.util.List;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author lordbobstein
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController
{
    @Autowired
    private IslandServices services;
    
    @Autowired
    private SchedulerFactoryBean scheduler;
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index()
    {
        ModelAndView model = new ModelAndView("/admin/index");
        List<Player> playerList = services.playerService.findAllPlayers();
        List<Archipelago> archipelagosList = services.archipelagService.findAllArchipelagos();
        
        model.addObject("playerList", playerList);
        model.addObject("archipelagosList", archipelagosList);
        
        return model;
    }

    @RequestMapping(value = "/newround", method = RequestMethod.POST)
    @ResponseBody
    public String NewRound(int archipelagoId) throws SchedulerException
    {
        String name = Integer.toString(archipelagoId);
        scheduler.getScheduler().triggerJob(new JobKey(name));
        
        return null;
    }
    
    @RequestMapping(value = "/pauseall", method = RequestMethod.POST)
    @ResponseBody
    public String pauseAll() throws SchedulerException
    {
        scheduler.getScheduler().pauseAll();
        
        return null;
    }
    
    @RequestMapping(value = "/startall", method = RequestMethod.POST)
    @ResponseBody
    public String startAll() throws SchedulerException
    {
        scheduler.getScheduler().resumeAll();
       
        return null;
    }
    
    @RequestMapping(value = "/gettime", method = RequestMethod.POST)
    @ResponseBody
    public String getTime(int archipelagoId) throws SchedulerException
    {
        Trigger trigger = scheduler.getScheduler().getTrigger(new TriggerKey(Integer.toString(archipelagoId)));
        Date nextFireTime = trigger.getNextFireTime();  
        
        return nextFireTime.toString();
    }
    
}


