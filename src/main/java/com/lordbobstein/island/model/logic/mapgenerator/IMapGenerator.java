/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.logic.mapgenerator;

import com.lordbobstein.island.model.entities.archipelago.Archipelago;

/**
 *
 * @author lordbobstein
 */
public interface IMapGenerator extends MapConstants
{    
    public void createNewMap(Archipelago archipelago); 
}
