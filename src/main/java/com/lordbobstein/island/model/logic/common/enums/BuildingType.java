package com.lordbobstein.island.model.logic.common.enums;


/**
 * Enumeration class TypyBudynkow - write a description of the enum class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
public enum BuildingType
{
    FARM, QUARRY, LUMBERJACK
}
