/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.common.skills;

import com.lordbobstein.island.model.entities.player.Player;

/**
 *
 * @author lordbobstein
 */
public class IncreaseWoodProduction implements Skill
{

    @Override
    public String getName()
    {
        return "Zwieksza produkcje drzewa";
    }

    @Override
    public void performAction(Player gracz)
    {
        gracz.getWood().improveIncrease(50);
    }
}
