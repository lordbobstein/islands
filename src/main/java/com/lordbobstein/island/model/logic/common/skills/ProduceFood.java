/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.logic.common.skills;

import com.lordbobstein.island.model.entities.player.Player;

/**
 *
 * @author lordbobstein
 */
public class ProduceFood implements Skill
{

    @Override
    public String getName()
    {
        return "Zapewnia dostawy jedzenia";
    }

    @Override
    public void performAction(Player gracz)
    {
        int jedzenieWartosc = gracz.getFood().getResourceValue();
        int jedzeniePrzyrost = gracz.getFood().getResourceIncrease();
        
        gracz.getFood().setResourceValue(jedzenieWartosc + jedzeniePrzyrost);        
    }
    
}
