/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.common;

import com.lordbobstein.island.model.dao.playersDAO.PlayerDAO;
import com.lordbobstein.island.model.dao.playersDAO.PlayerDataDAO;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.entities.player.PlayerData;
import java.util.HashSet;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author lordbobstein
 */
@Component
public class MyShiroRealm extends AuthorizingRealm
{

    @Autowired
    private PlayerDataDAO playerDataDAO;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals)
    {
        PlayerData gracz = (PlayerData) getAvailablePrincipal(principals);

        Set roleNames = new HashSet();

        roleNames.add(gracz.getPlayerType());
        Set permissions = new HashSet();

        SimpleAuthorizationInfo simpleAuthInfo = new SimpleAuthorizationInfo(roleNames);

        simpleAuthInfo.setStringPermissions(permissions);
        return simpleAuthInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException
    {
        PlayerData playerData = null;
        UsernamePasswordToken tk = (UsernamePasswordToken) token;
        try
        {
            playerData = playerDataDAO.findByLogin(tk.getUsername());            
        }
        catch (AuthenticationException ex)
        {
            System.out.println(ex);
            throw new AuthenticationException(ex);
        }

        return new SimpleAuthenticationInfo(playerData.getLogin(), playerData.getPassword(), getName());

    }

}
