package com.lordbobstein.island.model.logic.buildings;

/**
 * Write a description of class Produkt2 here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
import com.lordbobstein.island.model.logic.factories.buildings.BuildingDomain;
import com.lordbobstein.island.model.logic.common.costs.Costs;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.logic.common.skills.IncreaseFoodProduction;
import com.lordbobstein.island.model.logic.common.skills.ProduceFood;
import java.util.ArrayList;

public class Farm extends BuildingDomain
{

    public Farm()
    {
        costs = new Costs(5, 10, 7, 3);        

        this.buildingType = BuildingType.FARM;
        this.level = 1;
        skills = new ArrayList();
        skills.add(new ProduceFood());
        skills.add(new IncreaseFoodProduction());
    }
}
