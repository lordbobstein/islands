package com.lordbobstein.island.model.logic.buildings;



import com.lordbobstein.island.model.logic.factories.buildings.BuildingDomain;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;

public abstract class IBuildingsFactory
{   
    public abstract BuildingDomain create(BuildingType typ, int level);    
}
