/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.common.skills;

import com.lordbobstein.island.model.entities.player.Player;

/**
 *
 * @author lordbobstein
 */
public class ProduceStone implements Skill
{

    @Override
    public String getName()
    {
        return "Zapewnia dostawy kamienia";
    }

    @Override
    public void performAction(Player gracz)
    {
        int value = gracz.getStone().getResourceValue();
        int increase = gracz.getStone().getResourceIncrease();
        
        gracz.getStone().setResourceValue(value + increase);        
    }
    
}
