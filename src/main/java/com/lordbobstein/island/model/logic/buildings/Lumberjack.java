/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.logic.buildings;

import com.lordbobstein.island.model.logic.factories.buildings.BuildingDomain;
import com.lordbobstein.island.model.logic.common.costs.Costs;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.logic.common.skills.IncreaseWoodProduction;
import com.lordbobstein.island.model.logic.common.skills.ProduceWood;
import java.util.ArrayList;

/**
 *
 * @author lordbobstein
 */
public class Lumberjack extends BuildingDomain
{
    public Lumberjack()
    {
        costs = new Costs(20, 10, 4, 3);
        
        this.buildingType = BuildingType.LUMBERJACK;
        this.level = 1;
        skills = new ArrayList();
        skills.add(new ProduceWood());
        skills.add(new IncreaseWoodProduction());
    }
}
