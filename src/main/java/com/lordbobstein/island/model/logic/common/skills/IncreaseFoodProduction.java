package com.lordbobstein.island.model.logic.common.skills;

import com.lordbobstein.island.model.entities.player.Player;

/**
 * Write a description of class ZwiekszProdukcjeJedzenia here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class IncreaseFoodProduction implements Skill
{
    @Override
    public String getName()
    {
        return "Zwieksza produkcje jedzenia";
    }

    @Override
    public void performAction(Player player)
    {
        player.getFood().improveIncrease(50);
    }
}
