/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.mapgenerator;

import com.lordbobstein.island.model.dao.islandsDAO.IslandDAO;
import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.entities.island.Island;
import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.logic.common.enums.IslandResourcesType;
import com.lordbobstein.island.model.logic.common.enums.IslandType;
import static com.lordbobstein.island.model.logic.mapgenerator.MapConstants.MAP_SIZE;
import static com.lordbobstein.island.model.logic.mapgenerator.MapConstants.MIN_GAP;
import java.util.ArrayList;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author lordbobstein
 */
@Component
public class DBMapGenerator implements IMapGenerator
{

    @Autowired
    private IslandDAO islandDAO;

    private Random random = new Random();
    private Integer angle = 0;
    private ArrayList<Location> points = new ArrayList<Location>();

    @Override
    public void createNewMap(Archipelago archipelago)
    {
        for (int i = 0; i < NUMBER_OF_ISLANDS; i++)
        {
            Island island = calculateNewIsland();
            island.setArchipelago(archipelago);
            island.setIslandType(IslandType.PLAYERS_ISLAND);
            islandDAO.save(island);
            angle += ANGLE;
        }

        for (int i = 0; i < NUMBER_OF_RESOURCES_ISLAND; i++)
        {
            Island island = calculateNewResourcesIsland();
            island.setArchipelago(archipelago);
            island.setIslandType(IslandType.RESOURCES_ISLAND);     
            islandDAO.save(island);
        }
    }

    

    private Island calculateNewIsland()
    {
        Location location;

        do
        {
            location = new Location(calculateX(), calculateY());
        }
        while (!isMinGap(location));
        points.add(location);

        return new Island(location, true);
    }

    private Island calculateNewResourcesIsland()
    {
        Location location;
        do
        {   
            int x = random.nextInt(99) + 1;
            int y = random.nextInt(99) + 1;
            location = new Location(x, y);
        }
        while (this.contains(location));
        points.add(location);

        Island island = new Island(location, true);
        int rand = random.nextInt(3);
        
        switch(rand)
        {
            case 0:
                island.setResourceType(IslandResourcesType.FOOD);
                break;
                
            case 1:
                island.setResourceType(IslandResourcesType.STONE);
                break;
                
            case 2:
                island.setResourceType(IslandResourcesType.WOOD);
                break;
        }
        
        return island;
    }

    private int calculateX()
    {
        float r = random.nextFloat() * 49 + 1;
        int x = (int) (MAP_SIZE / 2 + r * Math.cos(angle));
        return x;
    }

    private int calculateY()
    {
        float r = random.nextFloat() * 49 + 1;
        int y = (int) (MAP_SIZE / 2 + r * Math.sin(angle));
        return y;
    }

    public boolean isMinGap(Location p)
    {
        for (Location point : points)
        {
            int a = (int) Math.pow(p.getX() - point.getX(), 2);
            int b = (int) Math.pow(p.getY() - point.getY(), 2);
            int distance = (int) Math.sqrt(a + b);
            if (distance < MIN_GAP)
            {
                return false;
            }
        }

        return true;
    }
    
    public boolean contains(Location location)
    {
        for (Location l : points)
        {
            if (l.getX() == location.getX() && l.getY() == location.getY())
            {
                return true;
            }
        }
        
        return false;
    }

}
