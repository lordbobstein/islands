package com.lordbobstein.island.model.logic.factories.buildings;


import com.lordbobstein.island.model.logic.common.costs.Costs;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.logic.common.skills.Skill;
import java.util.ArrayList;


public abstract class BuildingDomain
{
    protected int level;    
    protected ArrayList<Skill> skills;
    protected Costs costs;
    protected boolean enoughResourcesToBuild;
    protected boolean built;
    protected BuildingType buildingType;

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }   

    public ArrayList<Skill> getSkills()
    {
        return skills;
    }

    public void setSkills(ArrayList<Skill> skills)
    {
        this.skills = skills;
    }

    public Costs getCosts()
    {
        return costs;
    }

    public void setCosts(Costs costs)
    {
        this.costs = costs;
    }

    public boolean isEnoughResourcesToBuild()
    {
        return enoughResourcesToBuild;
    }

    public void setEnoughResourcesToBuild(boolean enoughResourcesToBuild)
    {
        this.enoughResourcesToBuild = enoughResourcesToBuild;
    }
    
    public int getCostFood()
    {
        return costs.getFoodCost();
    }
    public int getCostStone()
    {
        return costs.getStoneCost();
    }
    public int getCostWood()
    {
        return costs.getWoodCost();
    }

    public boolean isBuilt()
    {
        return built;
    }

    public void setBuilt(boolean built)
    {
        this.built = built;
    }

    public BuildingType getBuildingType()
    {
        return buildingType;
    }

    public void setBuildingType(BuildingType buildingType)
    {
        this.buildingType = buildingType;
    }
      
}
