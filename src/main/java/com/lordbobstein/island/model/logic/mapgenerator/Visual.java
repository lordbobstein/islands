/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.mapgenerator;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author lordbobstein
 */
public class Visual extends JFrame implements MapConstants
{
    
    
    public Visual()
    {
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1000, 1000);
    }

    private Point[] readMap(String path) throws ParserConfigurationException, IOException, SAXException
    {
        Point[] points = new Point[NUMBER_OF_ISLANDS];

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document doc = documentBuilder.parse(new File(path));

        Element root = doc.getDocumentElement();
        NodeList nodeList = root.getChildNodes();
        for (int i = 0; i < NUMBER_OF_ISLANDS; i++)
        {
            Node node = nodeList.item(i);
            
            Element e = (Element) node;
            Element ex = (Element) e.getFirstChild();
            Element ey = (Element) e.getLastChild();

            Text tx = (Text) ex.getFirstChild();
            String x = tx.getData().trim();

            tx = (Text) ey.getFirstChild();
            String y = tx.getData().trim();

            points[i] = new Point(Integer.parseInt(x), Integer.parseInt(y));

        }

        return points;
    }

    public boolean containsPoint(Point[] points, Point p)
    {
        for (int i = 0; i < NUMBER_OF_ISLANDS; i++)
        {
            if (points[i].equals(p))
            {
                return true;
            }
        }

        return false;
    }

    public void visualMap(String path) throws ParserConfigurationException, IOException, SAXException
    {
        GridLayout layout = new GridLayout(100, 100);
        this.setLayout(layout);

        Point[] points = readMap(path);

        for (int i = 0; i < MAP_SIZE; i++)
        {
            for (int j = 0; j < MAP_SIZE; j++)
            {
                JPanel b = new JPanel();
                b.setBackground(Color.WHITE);
                if (containsPoint(points, new Point(j, i)))
                {
                    b.setBackground(Color.RED);                    
                }
                if (i == MAP_SIZE/2 && j == MAP_SIZE/2)
                {
                    b.setBackground(Color.BLUE);
                }
                
                this.add(b);
            }
        }

    }

}
