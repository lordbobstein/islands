/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.common.costs;

import com.google.common.math.IntMath;
import java.math.RoundingMode;

/**
 *
 * @author lordbobstein
 */
public class Costs implements ICosts
{
    private int foodCost;
    private int woodCost;
    private int stoneCost;
    
    private int multiplier;

    public Costs(int foodCost, int woodCost, int stoneCost, int multiplier)
    {
        this.foodCost = foodCost;
        this.woodCost = woodCost;
        this.stoneCost = stoneCost;
        this.multiplier = multiplier;
    }
    
    

    @Override
    public void calculateCosts(int level)
    {
        for (int i = 0; i < level - 1; i++)
        {
            foodCost += foodCost * IntMath.sqrt(multiplier, RoundingMode.UP);
            woodCost += woodCost * IntMath.sqrt(multiplier, RoundingMode.UP);
            stoneCost += stoneCost * IntMath.sqrt(multiplier, RoundingMode.UP);
        }
    }

    public int getFoodCost()
    {
        return foodCost;
    }

    public void setFoodCost(int foodCost)
    {
        this.foodCost = foodCost;
    }

    public int getWoodCost()
    {
        return woodCost;
    }

    public void setWoodCost(int woodCost)
    {
        this.woodCost = woodCost;
    }

    public int getStoneCost()
    {
        return stoneCost;
    }

    public void setStoneCost(int stoneCost)
    {
        this.stoneCost = stoneCost;
    }

    public int getMultiplier()
    {
        return multiplier;
    }

    public void setMultiplier(int multiplier)
    {
        this.multiplier = multiplier;
    }
    
    
    
}
