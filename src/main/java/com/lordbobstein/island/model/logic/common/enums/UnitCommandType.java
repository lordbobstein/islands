/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.common.enums;

/**
 *
 * @author lordbobstein
 */
public enum UnitCommandType
{
    STOP, RETURN, NEW_COURSE
}
