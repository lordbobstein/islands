package com.lordbobstein.island.model.logic.buildings;


import com.lordbobstein.island.model.logic.factories.buildings.BuildingDomain;
import com.lordbobstein.island.model.logic.common.costs.Costs;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.logic.common.skills.IncreaseStoneProduction;
import com.lordbobstein.island.model.logic.common.skills.ProduceStone;
import java.util.ArrayList;

public class Quarry extends BuildingDomain
{
    public Quarry()
    {
        costs = new Costs(35, 5, 5, 2);
        
        this.buildingType = BuildingType.QUARRY;
        this.level = 1;      
        skills = new ArrayList();
        skills.add(new ProduceStone());
        skills.add(new IncreaseStoneProduction());
    }
}
