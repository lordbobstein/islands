/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.common.costs;

/**
 *
 * @author lordbobstein
 */
public interface ICosts
{
    void calculateCosts(int level);    
    int getFoodCost();
    int getWoodCost();
    int getStoneCost();
    int getMultiplier();    
}
