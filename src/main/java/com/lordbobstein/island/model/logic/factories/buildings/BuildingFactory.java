package com.lordbobstein.island.model.logic.factories.buildings;

import com.lordbobstein.island.model.logic.buildings.Farm;
import com.lordbobstein.island.model.logic.buildings.Lumberjack;
import com.lordbobstein.island.model.logic.buildings.Quarry;
import com.lordbobstein.island.model.logic.buildings.IBuildingsFactory;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.logic.common.skills.IncreaseFoodProduction;
import com.lordbobstein.island.model.logic.common.skills.IncreaseStoneProduction;
import com.lordbobstein.island.model.logic.common.skills.IncreaseWoodProduction;
import org.springframework.stereotype.Component;

@Component
public class BuildingFactory extends IBuildingsFactory
{

    @Override
    public BuildingDomain create(BuildingType buildingType, int level)
    {
        BuildingDomain building = null;

        if (buildingType == BuildingType.FARM)
        {
            building = new Farm();
        }
        else if (buildingType == BuildingType.QUARRY)
        {
            building = new Quarry();
        }
        else if (buildingType == BuildingType.LUMBERJACK)
        {
            building = new Lumberjack();
        }

        building.setLevel(level);
        
        for (int i = 0; i < level - 1; i++)
        {
            building = upgrade(building);
        }
        
        building.getCosts().calculateCosts(level);

        return building;
    }

    private BuildingDomain upgrade(BuildingDomain building)
    {
        BuildingType type = building.getBuildingType();

        switch (type)
        {
            case FARM:
                building.getSkills().add(new IncreaseFoodProduction());
                break;

            case LUMBERJACK:
                building.getSkills().add(new IncreaseWoodProduction());
                break;

            case QUARRY:
                building.getSkills().add(new IncreaseStoneProduction());
                break;

            default:
                return null;
        }
        
        return building;
    }
}
