/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.common.skills;

import com.lordbobstein.island.model.entities.player.Player;

/**
 *
 * @author lordbobstein
 */
public class ProduceWood implements Skill
{
    @Override
    public String getName()
    {
        return "Zapewnia dostawy drzewa";
    }

    @Override
    public void performAction(Player gracz)
    {
        int value = gracz.getWood().getResourceValue();
        int increase = gracz.getWood().getResourceIncrease();
        
        gracz.getWood().setResourceValue(value + increase);        
    }
    
}
