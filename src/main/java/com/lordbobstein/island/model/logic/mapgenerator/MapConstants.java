/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.logic.mapgenerator;

/**
 *
 * @author lordbobstein
 */
public interface MapConstants
{
    final int MAP_SIZE = 100;    
    final int NUMBER_OF_ISLANDS = 8;
    final int ANGLE = 360/NUMBER_OF_ISLANDS;
    final int MIN_GAP = 10; 
    final int NUMBER_OF_RESOURCES_ISLAND = 25;
}
