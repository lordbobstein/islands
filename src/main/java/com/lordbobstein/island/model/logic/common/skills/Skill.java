package com.lordbobstein.island.model.logic.common.skills;

import com.lordbobstein.island.model.entities.player.Player;




/**
 * Write a description of interface Umiejetnosc here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public interface Skill
{
    public String getName();
    public void performAction(Player player);
}
