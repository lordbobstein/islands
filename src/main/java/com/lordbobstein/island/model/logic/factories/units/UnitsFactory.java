/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.factories.units;

import com.lordbobstein.island.model.entities.unit.Trader;
import com.lordbobstein.island.model.entities.unit.Unit;
import com.lordbobstein.island.model.entities.unit.Warship;
import com.lordbobstein.island.model.logic.common.enums.UnitType;
import org.springframework.stereotype.Component;

/**
 *
 * @author lordbobstein
 */
@Component
public class UnitsFactory implements IUnitsFactory
{
  @Override
  public Unit create(UnitType type)   
    {
        if (type == UnitType.TRADER)
        {
            return new Trader();
        }
        else if (type == UnitType.WARSHIP)
        {
            return new Warship();
        }
        else
        {
            return null;
        }
    }  
  
  
}
