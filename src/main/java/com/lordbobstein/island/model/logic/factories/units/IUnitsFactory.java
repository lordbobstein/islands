/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.logic.factories.units;

import com.lordbobstein.island.model.entities.unit.Unit;
import com.lordbobstein.island.model.logic.common.enums.UnitType;

/**
 *
 * @author lordbobstein
 */
public interface IUnitsFactory
{
    Unit create(UnitType type); 
}
