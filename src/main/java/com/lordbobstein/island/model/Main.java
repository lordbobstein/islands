package com.lordbobstein.island.model;

import java.net.URL;
import java.net.URLClassLoader;
import static javax.servlet.SessionTrackingMode.URL;


public class Main
{
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {             
        ClassLoader cl = ClassLoader.getSystemClassLoader();
 
        URL[] urls = ((URLClassLoader)cl).getURLs();
 
        for(URL url: urls){
        	System.out.println(url.getFile());
        }
      
       
        
        
    }
}
