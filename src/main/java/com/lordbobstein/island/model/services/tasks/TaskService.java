/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.tasks;

import com.lordbobstein.island.model.dao.playersDAO.PlayerDAO;
import com.lordbobstein.island.model.dao.tasksDAO.TaskDAO;
import com.lordbobstein.island.model.entities.building.Building;
import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.entities.task.Task;
import com.lordbobstein.island.model.entities.unit.Unit;
import com.lordbobstein.island.model.logic.buildings.IBuildingsFactory;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.logic.common.enums.TaskType;
import com.lordbobstein.island.model.logic.common.enums.UnitType;
import com.lordbobstein.island.model.logic.factories.buildings.BuildingDomain;
import com.lordbobstein.island.model.logic.factories.units.IUnitsFactory;
import com.lordbobstein.island.model.services.archipelagos.IArchipelagosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lordbobstein
 */
@Service
public class TaskService implements ITaskService
{

    @Autowired
    private TaskDAO taskDAO;

    @Autowired
    private PlayerDAO playerDAO;

    @Autowired
    private IArchipelagosService archipelagosService;

    @Autowired
    private IUnitsFactory unitsFactory;

    @Autowired
    private IBuildingsFactory buildingsFactory;

    @Override
    public void createBuildOrUpgradeBuildingTask(String login, BuildingType type)
    {
        Player player = playerDAO.findByPlayerData_Login(login);

        int food = player.getFood().getResourceValue();
        int stone = player.getStone().getResourceValue();
        int wood = player.getWood().getResourceValue();

        Building b = player.getBuildings().getOrDefault(type, null);
        BuildingDomain building;

        if (b == null)
        {
            building = buildingsFactory.create(type, 1);
        }
        else
        {
            building = buildingsFactory.create(type, b.getPoziom() + 1);
        }

        player.getFood().setResourceValue(food - building.getCostFood());
        player.getStone().setResourceValue(stone - building.getCostStone());
        player.getWood().setResourceValue(wood - building.getCostWood());
        player.setIsBuildingInQueue(true);
        playerDAO.save(player);

        Task task = new Task();
        task.setPlayer(player);
        task.setTaskType(TaskType.BUILD_OR_UPGRADE_BUILDING);
        task.setTargetsName(type.name());
        taskDAO.save(task);
    }

    @Override
    public void createBuildNewUnitTask(String login, UnitType type)
    {
        Player player = playerDAO.findByPlayerData_Login(login);

        Unit unit = unitsFactory.create(type);

        int food = player.getFood().getResourceValue();
        int stone = player.getStone().getResourceValue();
        int wood = player.getWood().getResourceValue();

        player.getFood().setResourceValue(food - unit.getCostFood());
        player.getStone().setResourceValue(stone - unit.getCostStone());
        player.getWood().setResourceValue(wood - unit.getCostWood());

        playerDAO.save(player);

        Task task = new Task();
        task.setPlayer(player);
        task.setTargetsName(type.name());
        task.setTaskType(TaskType.NEW_UNIT);
        int stage = archipelagosService.getCurrentStage();
        task.setStageId(stage);
        task.setDone(false);

        taskDAO.save(task);
    }

    @Override
    public void createMoveUnitTask(String login, int unitId, Location location)
    {
        Player player = playerDAO.findByPlayerData_Login(login);

        Task task = new Task();
        task.setTargetsName(Integer.toString(unitId));
        task.setDone(false);
        task.setTaskType(TaskType.MOVE_UNIT);
        task.setPlayer(player);
        int stage = archipelagosService.getCurrentStage();
        task.setStageId(stage);

        taskDAO.save(task);

        task.setLocation(location);
        taskDAO.save(task);
    }
}
