/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.services.game.commands;

import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.services.buildings.IBuildingsService;

/**
 *
 * @author lordbobstein
 */

public class BuildOrUpgradeBuilding implements ICommand
{    
    
    private IBuildingsService buildingsService;
    
    private Player player;
    private String targetsName;

    public BuildOrUpgradeBuilding(IBuildingsService buildingsService)
    {
        this.buildingsService = buildingsService;
    }
    
    @Override
    public void setData(Player player, String targetsName)
    {
        this.player = player;
        this.targetsName = targetsName;
    }
   
    @Override
    public void execute()
    {
        BuildingType typ = BuildingType.valueOf(targetsName);
        
        buildingsService.buildOrUpgradeBuilding(player, typ);
    }

    @Override
    public void setData(int unitId, Location location)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    
}
