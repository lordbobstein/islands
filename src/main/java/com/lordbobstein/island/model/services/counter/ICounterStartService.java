/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.counter;

import org.quartz.SchedulerException;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 *
 * @author lordbobstein
 */
public interface ICounterStartService extends ApplicationListener<ContextRefreshedEvent>
{
    public void onApplicationEvent(ContextRefreshedEvent event);
    public void addNewJobForNewArchipelago(int archipelagoId);
    
}
