package com.lordbobstein.island.model.services.players;

import com.lordbobstein.island.model.dao.playersDAO.PlayerDAO;
import com.lordbobstein.island.model.entities.island.Island;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.entities.player.PlayerData;
import com.lordbobstein.island.model.logic.common.enums.PlayerType;
import com.lordbobstein.island.model.services.archipelagos.IArchipelagosService;
import com.lordbobstein.island.ui.viewmodel.DashboardViewModel;
import java.util.List;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lordbobstein
 */
@Service
@Transactional
public class PlayerService implements IPlayerService
{

    @Autowired
    private PlayerDAO graczDAO;  //<--spring 

    @Autowired
    private IArchipelagosService archipelagService; //<--spring 

    /**
     * Tworzy nowego gracza sprawdzajac wczesniej czy juz taki login istnieje
     *
     * @param login
     * @param haslo
     * @return prawda - gracz utworzony false - gracz juz istnial
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @Override
    public boolean createPlayer(String login, String haslo)
    {
        if (graczDAO.findByPlayerData_Login(login) == null)
        {
            //Tworzy nowe dane gracza
            PlayerData playerData = new PlayerData();
            playerData.setLogin(login);
            playerData.setPassword(haslo);
            playerData.setPlayerType(PlayerType.USER);

            //Pobiera nowa wyspe
            Island nowaWyspa = archipelagService.getNewIsland();
            nowaWyspa.setIsFree(false);

            //Tworzy nowego gracza
            Player nowyGracz = new Player(playerData);

            //Zapisuje gracza do bazy danych -persist
            Player player = graczDAO.save(nowyGracz);
            player.setArchipelago(nowaWyspa.getArchipelago());
            player.setPlayersIsland(nowaWyspa);

            player.getFood().setResourceValue(2000);
            player.getStone().setResourceValue(2000);
            player.getWood().setResourceValue(2000);

            nowaWyspa.setOwner(player);

            graczDAO.save(player); //merge

            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public Player getPlayerByLogin(String login)
    {
        return graczDAO.findByPlayerData_Login(login);
    }

    @Override
    public List<Player> findAllPlayers()
    {
        return graczDAO.findAll();
    }

    @Override
    public int countPlayerIsland(String login)
    {
        return graczDAO.findByPlayerData_Login(login).getIslands().size();
    }

    @Override
    public List<Player> findAndSortPlayersByNumberOfIsland(int archipelagoId)
    {
        return graczDAO.findAndSortByNumberOfIsland(archipelagoId);
    }

    

}
