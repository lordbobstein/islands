/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.units;

import com.google.common.math.IntMath;
import com.lordbobstein.island.model.dao.archipelagosDAO.ArchipelagosDAO;
import com.lordbobstein.island.model.dao.playersDAO.PlayerDAO;
import com.lordbobstein.island.model.dao.unitsDAO.UnitDAO;
import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.unit.Unit;
import com.lordbobstein.island.model.logic.common.enums.IslandResourcesType;
import com.lordbobstein.island.model.logic.common.enums.UnitState;
import com.lordbobstein.island.model.logic.factories.units.IUnitsFactory;
import com.lordbobstein.island.model.logic.common.enums.UnitType;
import com.lordbobstein.island.ui.viewmodel.UnitsTypeViewModel;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lordbobstein
 */
@Service
public class UnitsService implements IUnitsService
{

    @Autowired
    private PlayerDAO playerDAO;

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private ArchipelagosDAO archipelagosDAO;

    @Autowired
    private IUnitsFactory unitsFactory;

    @Override
    public boolean checkIfIsPossibleToBuildNewUnit(UnitType type, String login)
    {
        Unit unit = unitsFactory.create(type);
        Player player = playerDAO.findByPlayerData_Login(login);
        int playersFood = player.getFood().getResourceValue();
        int playersStone = player.getStone().getResourceValue();
        int playersWood = player.getWood().getResourceValue();

        int foodCost = unit.getCostFood();
        int stoneCost = unit.getCostStone();
        int woodCost = unit.getCostWood();

        if (playersFood < foodCost)
        {
            return false;
        }
        else if (playersStone < stoneCost)
        {
            return false;
        }
        else if (playersWood < woodCost)
        {
            return false;
        }

        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void buildNewUnit(Player player, UnitType type)
    {
        int x = player.getPlayersIsland().getLocation().getX();
        int y = player.getPlayersIsland().getLocation().getY();

        int archipelagoId = player.getPlayersIsland().getArchipelagoId();
        Archipelago archipelago = archipelagosDAO.findById(archipelagoId);

        Location startLocation = new Location(x, y);

        Unit unit = unitsFactory.create(type);
        
        unit.setArchipelago(archipelago);        
        unit.setCurrentLocation(startLocation);
        unit.setDestinationLocation(startLocation);
        unit.setInMove(false);
        unit.setOwner(player);
        unit.setUnitState(UnitState.ALIVE);

        unitDAO.save(unit);
    }

    @Override
    public Location getCurrentLocation(int unitId)
    {
        Unit unit = unitDAO.findOne(unitId);

        return unit.getCurrentLocation();
    }

    @Override
    public Location getLocationOfPort(int unitId)
    {
        Location location = unitDAO.findOne(unitId).getOwner().getPlayersIsland().getLocation();
        int x = location.getX();
        int y = location.getY();

        return new Location(x, y);
    }

    @Override
    public boolean checkIfTheUnitIsInThePort(Unit unit)
    {
        if (unit.isInMove())
        {
            return false;
        }
        else
        {
            int x = unit.getCurrentLocation().getX();
            int y = unit.getCurrentLocation().getY();

            int xOfThePort = unit.getOwner().getPlayersIsland().getLocation().getX();
            int yOfThePort = unit.getOwner().getPlayersIsland().getLocation().getY();

            if (x == xOfThePort && y == yOfThePort)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    @Override
    public int getArchipelagoId(int unitId)
    {
        return unitDAO.findOne(unitId).getArchipelago().getId();
    }

    @Override
    public Unit getUnitById(int unitId)
    {
        return unitDAO.findOne(unitId);
    }

    private double calculateDistance(Location currentLocation, Location destinationLocation)
    {
        int x = currentLocation.getX() - destinationLocation.getX();
        int y = currentLocation.getY() - destinationLocation.getY();
        x = IntMath.pow(x, 2);
        y = IntMath.pow(y, 2);
        return Math.sqrt(x + y);
    }

    @Override
    public int createNewCourseData(int unitId, Location location, IslandResourcesType resourceType)
    {
        Unit unit = unitDAO.findOne(unitId);
        unit.setResourceType(resourceType);
        unit.setDestinationLocation(location);
        unit.setInMove(true);
        
        int requiredStages = unit.calculateRequiredStages();
        unit.setRequiredStages(requiredStages);
        unitDAO.save(unit);

        return requiredStages;
    }

    @Override
    public int createNewCourseData(int unitId, Location location)
    {
        Unit unit = unitDAO.findOne(unitId);

        unit.setDestinationLocation(location);
        unit.setInMove(true);
        
        int requiredStages = unit.calculateRequiredStages();
        unit.setRequiredStages(requiredStages);
        unitDAO.save(unit);

        return requiredStages;
    }

    @Override
    public List<UnitsTypeViewModel> prepareUnitsTypeList(String login)
    {
        List<UnitsTypeViewModel> types = new ArrayList<>();

        boolean enoughtResourcesToBuildTrader = checkIfIsPossibleToBuildNewUnit(UnitType.TRADER, login);
        Unit unit = unitsFactory.create(UnitType.TRADER);
        unit.setEnoughResourcesToBuild(enoughtResourcesToBuildTrader);
        types.add(new UnitsTypeViewModel(unit));

        enoughtResourcesToBuildTrader = checkIfIsPossibleToBuildNewUnit(UnitType.WARSHIP, login);
        unit = unitsFactory.create(UnitType.WARSHIP);
        unit.setEnoughResourcesToBuild(enoughtResourcesToBuildTrader);
        types.add(new UnitsTypeViewModel(unit));
        
        return types;
    }

}
