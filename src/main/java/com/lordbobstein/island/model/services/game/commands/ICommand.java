/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.services.game.commands;

import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.player.Player;

/**
 *
 * @author lordbobstein
 */
public interface ICommand
{
    public void setData(Player player, String targetsName);
    public void setData(int unitId, Location location);
    
    public void execute();
}
