/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.game.commands;

import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.logic.common.enums.UnitType;
import com.lordbobstein.island.model.services.units.IUnitsService;

/**
 *
 * @author lordbobstein
 */
public class BuildUnitCommand implements ICommand
{
    private IUnitsService services;
    
    private Player player;
    private String targetsName;

    public BuildUnitCommand(IUnitsService services)
    {
        this.services = services;
    }
    
    @Override
    public void setData(Player player, String targetsName)
    {
        this.player = player;
        this.targetsName = targetsName;
    }

    @Override
    public void execute()
    {
        UnitType type = UnitType.valueOf(targetsName);
        services.buildNewUnit(player, type);
    }

    @Override
    public void setData(int unitId, Location location)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
