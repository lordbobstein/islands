package com.lordbobstein.island.model.services.units;

import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.entities.unit.Unit;
import com.lordbobstein.island.model.logic.common.enums.IslandResourcesType;
import com.lordbobstein.island.model.logic.common.enums.UnitType;
import com.lordbobstein.island.ui.viewmodel.UnitsTypeViewModel;
import java.util.List;

/**
 *
 * @author lordbobstein
 */
public interface IUnitsService
{
     
    boolean checkIfIsPossibleToBuildNewUnit(UnitType type, String login);
    void buildNewUnit(Player player, UnitType type);
    Location getCurrentLocation(int unitId);
    Location getLocationOfPort(int unitId);
    boolean checkIfTheUnitIsInThePort(Unit unit);
    int getArchipelagoId(int unitId);
    Unit getUnitById(int unitId);
    int createNewCourseData(int unitId, Location location);
    int createNewCourseData(int unitId, Location location, IslandResourcesType resourceType);
    List<UnitsTypeViewModel> prepareUnitsTypeList(String login);
}
