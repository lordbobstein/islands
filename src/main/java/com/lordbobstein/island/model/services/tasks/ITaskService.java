/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.tasks;

import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.logic.common.enums.UnitType;

/**
 *
 * @author lordbobstein
 */
public interface ITaskService
{
    public void createBuildOrUpgradeBuildingTask(String login, BuildingType type);
    
    public void createBuildNewUnitTask(String login, UnitType type);
    
    public void createMoveUnitTask(String login, int unitId, Location location);
    
    
}
