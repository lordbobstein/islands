/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.game;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lordbobstein
 */
@Component
public class NewRoundJob implements Job
{
    @Autowired
    private GameService gameService;    
    
    @Transactional(rollbackFor = Exception.class)
    public void executeNewRound(int archipelagoId)
    {    
        gameService.prepareToNewRound(archipelagoId);
        gameService.executePlayersTasks(archipelagoId);
        //gameService.executeAbilitiesOfBuildings(archipelagoId);
        gameService.moveUnits(archipelagoId);
        gameService.battles(archipelagoId);
        gameService.newRound(archipelagoId);        
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        
        String a = dataMap.getString("id");
        int archipelagoId = Integer.valueOf(a);
        executeNewRound(archipelagoId);
    }
    
    
}
