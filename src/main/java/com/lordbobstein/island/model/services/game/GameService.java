/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.game;

import com.lordbobstein.island.model.services.game.commands.ICommand;
import com.lordbobstein.island.model.dao.archipelagosDAO.ArchipelagosDAO;
import com.lordbobstein.island.model.dao.islandsDAO.IslandDAO;
import com.lordbobstein.island.model.dao.playersDAO.PlayerDAO;
import com.lordbobstein.island.model.dao.tasksDAO.TaskDAO;
import com.lordbobstein.island.model.dao.unitsDAO.UnitDAO;
import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.entities.island.Island;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.entities.task.Task;
import com.lordbobstein.island.model.entities.unit.IUnitsVisitor;
import com.lordbobstein.island.model.entities.unit.Unit;
import com.lordbobstein.island.model.logic.common.enums.IslandType;
import com.lordbobstein.island.model.logic.common.enums.UnitState;
import com.lordbobstein.island.model.services.archipelagos.IArchipelagosService;
import com.lordbobstein.island.model.services.buildings.IBuildingsService;
import com.lordbobstein.island.model.services.game.commands.BuildOrUpgradeBuilding;
import com.lordbobstein.island.model.services.game.commands.BuildUnitCommand;
import com.lordbobstein.island.model.services.units.IUnitsService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

/**
 *
 * @author lordbobstein
 */
@Component
public class GameService implements IGameService
{

    @Autowired
    private PlayerDAO playerDAO;

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private TaskDAO taskDAO;

    @Autowired
    private IslandDAO islandDAO;

    @Autowired
    private ArchipelagosDAO archipelagosDAO;

    @Autowired
    private IArchipelagosService archipelagosService;

    @Autowired
    private IUnitsVisitor unitsVisitor;

    @Autowired
    private IUnitsService unitsService;

    @Autowired
    private IBuildingsService buildingsService;

    @Autowired
    private SchedulerFactoryBean scheduler;

    @Override
    public void prepareToNewRound(int archipelagoId)
    {
        List<Player> playerList = playerDAO.findByArchipelagoId(archipelagoId);

        for (Player player : playerList)
        {
            player.getFood().setResourceIncrease(0);
            player.getWood().setResourceIncrease(0);
            player.getStone().setResourceIncrease(0);

            playerDAO.save(player);
        }
    }

    @Override
    public void executePlayersTasks(int archipelagoId)
    {
        List<ICommand> tasks = createTasks(archipelagoId);
        System.out.println();

        for (ICommand p : tasks)
        {
            p.execute();
        }
    }

    @Override
    public void executeAbilitiesOfBuildings(int archipelagoId)
    {
        for (Player g : playerDAO.findByArchipelagoId(archipelagoId))
        {
            buildingsService.wykonajUmiejetnosciBudynkow(g);
        }
    }

    @Override
    public void moveUnits(int archipelagoId)
    {
        List<Unit> unitList = unitDAO.findByInMoveAndArchipelagoId(true, archipelagoId);

        for (Unit unit : unitList)
        {
            unit.moveUnit(unitDAO, islandDAO, unitsVisitor);
        }
    }

    @Override
    public void battles(int archipelagoId)
    {
        List<Island> islandList = islandDAO.findByArchipelago_IdAndIslandType(archipelagoId, IslandType.RESOURCES_ISLAND);
        List<Unit> warshipsList;
        Map<Integer, Integer> playersUnitWarships;

        for (Island island : islandList)
        {
            int x = island.getLocation().getX();
            int y = island.getLocation().getY();
            warshipsList = unitDAO.findWarshipsByInMoveAndCurrentLocation_xAndCurrentLocation_y(false, x, y);

            if (warshipsList.isEmpty())
            {
                continue;
            }

            playersUnitWarships = new HashMap<>();

            for (Unit unit : warshipsList)
            {
                Integer playerId = unit.getOwner().getId();
                Integer count = playersUnitWarships.get(playerId);
                playersUnitWarships.put(playerId, count == null ? 1 : count + 1);
            }

            /**
             * Przy wyspie znajduje sie tylko jeden gracz z okretami wojennymi
             *
             */
            if (playersUnitWarships.size() == 1)
            {
                Player player = warshipsList.get(0).getOwner();
                island.setOwner(player);
                islandDAO.save(island);
                continue;
            }

            List<Map.Entry<Integer, Integer>> entries = new ArrayList<>(playersUnitWarships.entrySet());
            Collections.sort(entries, new Comparator<Map.Entry<Integer, Integer>>()
            {
                @Override
                public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2)
                {
                    return Integer.compare(o2.getValue(), o1.getValue());
                }
            });

            int max = entries.get(0).getValue();
            int nextPlayerValue = entries.get(1).getValue();

            /**
             * Jesli jest wiecej niz jeden gracz posiadajacy najwieksza liczbę
             * jednostek, wszystkie jednostki znajdujące się przy tej wyspie
             * zostają zniszczone
             */
            if (nextPlayerValue == max)
            {
                List<Unit> tradersList = unitDAO.
                        findTradersByInMoveAndCurrentLocation_xAndCurrentLocation_y(false, x, y);

                unitDAO.delete(warshipsList);
                unitDAO.delete(tradersList);
            }
            else
            {
                /**
                 * Z wygranej floty zostaje zniszczona polowa z zaokragleniem w
                 * gore
                 */

                int winplayerId = entries.get(0).getKey();
                int warshipNumber;
                int warshipsToDestroyNumber;
                List<Unit> unitsToDestroy = unitDAO.findByInMoveAndCurrentLocation_xAndCurrentLocation_yAndNotOwner_id(false, x, y, winplayerId);

                for(Unit unit : unitsToDestroy)
                {
                    unit.setUnitState(UnitState.DESTROYED);
                }                
                unitDAO.save(unitsToDestroy);

                List<Unit> wonPlayerUnits = unitDAO.findWarshipsByInMoveAndCurrentLocation_xAndCurrentLocation_yAndOwner_id(false, x, y, winplayerId);
                warshipNumber = wonPlayerUnits.size();
                warshipsToDestroyNumber = (warshipNumber + 1) / 2;

                for (int i = 0; i < warshipsToDestroyNumber; i++)
                {
                    wonPlayerUnits.get(i).setUnitState(UnitState.DESTROYED);
                }
                unitDAO.save(wonPlayerUnits);

                Player player = wonPlayerUnits.get(0).getOwner();
                island.setOwner(player);
                islandDAO.save(island);
            }

        }
    }

    @Override
    public void newRound(int archipelagoId)
    {
        Archipelago archipelago = archipelagosDAO.findOne(archipelagoId);
        archipelago.setStage(archipelago.getStage() + 1);
        archipelagosDAO.save(archipelago);
    }

    @Override
    public int getNextRoundTime()
    {
        String archipelagoId = Integer.toString(archipelagosService.getCurrentPlayerArchipelagoId());
        Date nextFire = null;
        try
        {
            nextFire = scheduler.getScheduler().
                    getTrigger(new TriggerKey(archipelagoId)).
                    getNextFireTime();
        }
        catch (SchedulerException ex)
        {
            Logger.getLogger(GameService.class.getName()).log(Level.SEVERE, null, ex);
        }

        int seconds = (int) ((nextFire.getTime() - new Date().getTime()) / 1000);

        return seconds;
    }

    private List<ICommand> createTasks(int archipelagoId)
    {
        List<Task> zadania = taskDAO.findByArchipelago(archipelagoId);

        List<ICommand> commands = new ArrayList<>();

        for (Task z : zadania)
        {
            ICommand command = null;
            Player player = z.getPlayer();
            String targetsName = z.getTargetsName();
            z.setDone(true);
            taskDAO.save(z);

            switch (z.getTaskType())
            {
                case BUILD_OR_UPGRADE_BUILDING:
                    ICommand buildBuildingCommand = new BuildOrUpgradeBuilding(buildingsService);
                    buildBuildingCommand.setData(player, targetsName);
                    command = buildBuildingCommand;
                    break;

                case NEW_UNIT:
                    ICommand buildUnitCommand = new BuildUnitCommand(unitsService);
                    buildUnitCommand.setData(player, targetsName);
                    command = buildUnitCommand;
                    break;
            }

            commands.add(command);
        }

        return commands;
    }
}
