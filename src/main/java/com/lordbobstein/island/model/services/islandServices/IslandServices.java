/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.services.islandServices;

import com.lordbobstein.island.model.services.archipelagos.IArchipelagosService;
import com.lordbobstein.island.model.services.buildings.IBuildingsService;
import com.lordbobstein.island.model.services.game.IGameService;
import com.lordbobstein.island.model.services.players.IPlayerService;
import com.lordbobstein.island.model.services.tasks.ITaskService;
import com.lordbobstein.island.model.services.units.IUnitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author piotr
 */
@Service
public class IslandServices
{
    @Autowired
    public IArchipelagosService archipelagService;
    
    @Autowired
    public IBuildingsService buildingsService;
    
    @Autowired
    public IGameService gameService;
    
    @Autowired
    public IPlayerService playerService;
    
    @Autowired
    public IUnitsService unitsService;
    
    @Autowired
    public ITaskService taskService;
}
