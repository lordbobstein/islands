package com.lordbobstein.island.model.services.archipelagos;

import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.entities.island.Island;
import java.util.List;

/**
 *
 * @author lordbobstein
 */
public interface IArchipelagosService
{
    /**
     * Zwraca dane nowej wyspy. W sytuacji zapelnienia archipelagu tworzy nowy.
     *
     * @return nowa wyspa
     */
    Island getNewIsland();
    Island getIslandById(int id);
    List<Island> findAllIslands();
    List<Archipelago> findAllArchipelagos();
    List<Island> getResourcesIslandInArchipelago(int archipelagoId);    
    int getCurrentStage();
    int getCurrentPlayerArchipelagoId();
    
}
