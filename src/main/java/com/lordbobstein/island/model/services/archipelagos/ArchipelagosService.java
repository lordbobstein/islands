/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.archipelagos;

import com.lordbobstein.island.model.dao.archipelagosDAO.ArchipelagosDAO;
import com.lordbobstein.island.model.dao.islandsDAO.IslandDAO;
import com.lordbobstein.island.model.dao.playersDAO.PlayerDAO;
import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.entities.island.Island;
import com.lordbobstein.island.model.logic.common.enums.IslandType;
import com.lordbobstein.island.model.logic.mapgenerator.DBMapGenerator;
import com.lordbobstein.island.model.services.counter.ICounterStartService;
import java.util.List;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 *
 * @author lordbobstein
 */
@Component
public class ArchipelagosService implements IArchipelagosService
{
    @Autowired
    private ArchipelagosDAO archipelagosDAO;
    
    @Autowired
    private IslandDAO islandDAO;
    
    @Autowired
    private PlayerDAO playerDAO;
    
    @Autowired
    private DBMapGenerator DBMapGenerator;  
    
    @Autowired
    private ICounterStartService counterStartService;    

    @Override
    public Island getNewIsland()
    {
        //Wyszukuje ostatni zapisany archipelag
        Archipelago daneArchipelagow = archipelagosDAO.findLast();        

        //Jesli nie ma jeszcze zadnego archipelagu
        if (daneArchipelagow == null)
        {
            createNewArchipelagosData();
            return islandDAO.findFirstFreeIsland();
        }       

        //Pobiera informacje o ilosci zapisanych wysp
        int iloscZasiedlonych = daneArchipelagow.getNumberOfIslands();

        //Jesli archipelag jest pelen tworzy nowy
        if (iloscZasiedlonych == 8)
        {
            createNewArchipelagosData();
            return islandDAO.findFirstFreeIsland();
        }
        else
        {
            //Okresla ilosc zapisanych wysp na jedna wiecej
            daneArchipelagow.setNumberOfIslands(iloscZasiedlonych + 1);
          
            archipelagosDAO.save(daneArchipelagow);

            //Zwraca nowa wyspe na podstawie sciezki do mapy i ID wyspy
            return islandDAO.findFirstFreeIsland();
        }
    }
    
    @Override
    public Island getIslandById(int id)
    {
        return islandDAO.findOne(id);
    }
    
    @Override
    public List<Island> findAllIslands()
    {
        return islandDAO.findAll();
    }
    
    @Override
    public List<Archipelago> findAllArchipelagos()
    {
        return archipelagosDAO.findAll();
    }
    @Override
    public List<Island> getResourcesIslandInArchipelago(int archipelagoId)
    {
        return islandDAO.findByArchipelago_IdAndIslandType(archipelagoId, IslandType.RESOURCES_ISLAND);
    }
    
    @Override
    public int getCurrentPlayerArchipelagoId()
    {
        Subject subject = SecurityUtils.getSubject();
        String login = (String) subject.getPrincipal();
        return playerDAO.findByPlayerData_Login(login).getArchipelago().getId();
    }
            
    @Override
    public int getCurrentStage()
    {
        Subject subject = SecurityUtils.getSubject();
        String login = (String) subject.getPrincipal();
        int stage = archipelagosDAO.getCurrentStageByPlayerLogin(login);

        return stage;
    }

    private void createNewArchipelagosData()
    {
        //Nowe dane archipelagu
        Archipelago daneNowegoArchipelagu = new Archipelago();  

        //Liczba wysp(graczy) w danym archipelagu zostaje ustawiona na 1
        daneNowegoArchipelagu.setNumberOfIslands(1);
        
        daneNowegoArchipelagu.setStage(1);
        
         //Zapisuje dane nowego archipelagu
        archipelagosDAO.save(daneNowegoArchipelagu);
        
        int archipelagoId = daneNowegoArchipelagu.getId();
        
        counterStartService.addNewJobForNewArchipelago(archipelagoId);
        //Tworzy nowa mape i pobiera sciezke do niej
        DBMapGenerator.createNewMap(daneNowegoArchipelagu);
    }

    
}
