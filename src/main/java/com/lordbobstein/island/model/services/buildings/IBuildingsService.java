package com.lordbobstein.island.model.services.buildings;

import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;

/**
 *
 * @author lordbobstein
 */
public interface IBuildingsService
{
    /** Dla wskazanego gracza wykonuje umiejetnosci budynkow
     * 
     * @param player 
     */
    public void wykonajUmiejetnosciBudynkow(Player player);
    public boolean sprawdzMozliwoscWybudowaniaBudynku(BuildingType type, int level, String login);
    public void buildOrUpgradeBuilding(Player player, BuildingType type);    
}
