package com.lordbobstein.island.model.services.game;

/**
 *
 * @author lordbobstein
 */
public interface IGameService
{
    /** Przygotowuje kopie bazy danych ze stanem przyszlym
     * 
     */
    public void prepareToNewRound(int archipelagoId);
    
    /** Usuwa wszystkie dane stanu obecnego i zamienia stan przyszly na obecny
     * 
     */
    public void newRound(int archipelagoId);
    
    /** Dla wszystkich graczy wykonuje umiejetnosci budynkow
     * 
     */    
    public void executeAbilitiesOfBuildings(int archipelagoId);
    
    public void moveUnits(int archipelagoId);
    
    public void executePlayersTasks(int archipelagoId);
    
    public int getNextRoundTime();
    
    public void battles(int archipelagoId);
}
