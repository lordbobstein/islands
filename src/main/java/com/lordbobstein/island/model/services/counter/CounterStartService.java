/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.counter;

import com.lordbobstein.island.model.dao.archipelagosDAO.ArchipelagosDAO;
import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.logic.common.AutowiringSpringBeanJobFactory;
import com.lordbobstein.island.model.services.game.NewRoundJob;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

/**
 *
 * @author lordbobstein
 */
@Component
public class CounterStartService implements ICounterStartService
{

    @Autowired
    private ArchipelagosDAO archipelagosDAO;

    @Autowired
    private SchedulerFactoryBean scheduler;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event)
    {
        try
        {
            Scheduler sch = scheduler.getScheduler();
            sch.start();

            AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
            jobFactory.setApplicationContext(applicationContext);
            sch.setJobFactory(jobFactory);

            createJobsForAllArchipelagos(sch);
        }
        catch (Exception ex)
        {
            Logger.getLogger(CounterStartService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void addNewJobForNewArchipelago(int archipelagoId)
    {
        Scheduler sch = scheduler.getScheduler();
        String name = Integer.toString(archipelagoId);
        createNewRoundJob(name, sch);
    }

    private void createJobsForAllArchipelagos(Scheduler sch) throws SchedulerException
    {
        for (Archipelago archipelago : archipelagosDAO.findAll())
        {
            String name = String.valueOf(archipelago.getId());
            createNewRoundJob(name, sch);
        }
    }
    
    private void createNewRoundJob(String name, Scheduler sch)
    {
        JobDetail job = newJob(NewRoundJob.class)
                .withIdentity(name)
                .build();

        job.getJobDataMap().put("id", name);

        Trigger trigger = newTrigger()
                .withIdentity(name)
                .startAt(new Date(System.currentTimeMillis() + 1 * 60 * 1000))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(1).repeatForever())
                .build();

        try
        {
            sch.scheduleJob(job, trigger);
        }
        catch (SchedulerException ex)
        {
            Logger.getLogger(CounterStartService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

}
