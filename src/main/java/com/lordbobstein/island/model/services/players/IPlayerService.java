package com.lordbobstein.island.model.services.players;

import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.ui.viewmodel.DashboardViewModel;
import java.util.List;

/**
 *
 * @author lordbobstein
 */
public interface IPlayerService
{   
    /** Tworzy nowego gracza
     * 
     * @param login 
     * @param password
     * @return true jesli udane false gracz juz istnieje
     */
    public boolean createPlayer(String login, String password); 
    
    /** Zwraca gracza o podanym loginie ze stanu przyszlego.
     * 
     * @param login
     * @return 
     */
    public Player getPlayerByLogin(String login);
    
    public List<Player> findAllPlayers();
    
    public int countPlayerIsland(String login);
    
    public List<Player> findAndSortPlayersByNumberOfIsland(int archipelagoId);
    
    
}
