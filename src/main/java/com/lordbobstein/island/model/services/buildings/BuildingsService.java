/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.buildings;

import com.lordbobstein.island.model.dao.playersDAO.PlayerDAO;
import com.lordbobstein.island.model.entities.building.Building;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.logic.factories.buildings.BuildingDomain;
import com.lordbobstein.island.model.logic.buildings.IBuildingsFactory;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import com.lordbobstein.island.model.logic.common.skills.Skill;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author lordbobstein
 */
@Component
public class BuildingsService implements IBuildingsService
{

    @Autowired
    private IBuildingsFactory buildingsFactory;

    @Autowired
    private PlayerDAO playerDAO;

    @Override
    public void wykonajUmiejetnosciBudynkow(Player player)
    {
        ArrayList<Building> listaBudynkowEncji = new ArrayList<>(player.getBuildings().values());
        ArrayList<BuildingDomain> listaBudynkow = new ArrayList<>();

        for (Building b : listaBudynkowEncji)
        {
            int buildingLevel = b.getPoziom();

            BuildingDomain budynekDomain = buildingsFactory.create(b.getTypBudynku(), buildingLevel);
            listaBudynkow.add(budynekDomain);
        }

        for (BuildingDomain b : listaBudynkow)
        {
            ArrayList<Skill> umiejetnosci = b.getSkills();

            for (int i = umiejetnosci.size() - 1; i >= 0; i--)
            {
                Skill skill = umiejetnosci.get(i);
                skill.performAction(player);
            }
        }

        playerDAO.save(player);
    }

    /**
     * Buduje nowy budynek
     *
     * @param login gracz dla ktorej budynek jest tworzony
     * @param type powstajacego budynku
     */
    @Override
    public void buildOrUpgradeBuilding(Player player, BuildingType type)
    {
        Building b = player.getBuildings().getOrDefault(type, null);
        Building building;

        if (b == null)
        {
            building = new Building();
            building.setPoziom(1);
            building.setTypBudynku(type);
            player.getBuildings().put(type, building);
        }
        else
        {
            b.setPoziom(b.getPoziom() + 1);
        }

        player.setIsBuildingInQueue(false);
        playerDAO.save(player);
    }

    @Override
    public boolean sprawdzMozliwoscWybudowaniaBudynku(BuildingType typ, int poziom, String login)
    {
        BuildingDomain budynek = buildingsFactory.create(typ, poziom);
        Player gracz = playerDAO.findByPlayerData_Login(login);
        int jedzenieGracza = gracz.getFood().getResourceValue();
        int kamienGracza = gracz.getStone().getResourceValue();
        int drzewoGracza = gracz.getWood().getResourceValue();

        int kosztJedzenie = budynek.getCostFood();
        int kosztKamien = budynek.getCostStone();
        int kosztDrzewo = budynek.getCostWood();

        if (jedzenieGracza < kosztJedzenie)
        {
            return false;
        }
        if (kamienGracza < kosztKamien)
        {
            return false;
        }
        if (drzewoGracza < kosztDrzewo)
        {
            return false;
        }

        return true;
    }

}
