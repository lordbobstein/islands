/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.services.game.commands;

import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.services.units.IUnitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author lordbobstein
 */
@Component
public class MoveUnitCommand implements ICommand
{
    
    private int unitId;
    private Location location;
    
    @Autowired
    private IUnitsService unitService;

    @Override
    public void setData(int unitId, Location location)
    {
        this.unitId = unitId;
        this.location = location;
    }

    @Override
    public void execute()
    {
        unitService.createNewCourseData(unitId, location);
    }

    @Override
    public void setData(Player player, String nazwaBudynku)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
