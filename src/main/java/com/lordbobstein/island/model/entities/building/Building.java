/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.entities.building;

import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/** Encja budynku
 *
 * @author lordbobstein
 */
@Entity
public class Building implements Serializable
{
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    private int poziom;
    
    @Enumerated(EnumType.STRING)
    private BuildingType typBudynku;

    public Building()
    {
    }
    
    public Building(int poziom, BuildingType typBudynku)
    {
        this.poziom = poziom;
        this.typBudynku = typBudynku;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return id;
    }    

    public int getPoziom()
    {
        return poziom;
    }

    public void setPoziom(int poziom)
    {
        this.poziom = poziom;
    }

    public BuildingType getTypBudynku()
    {
        return typBudynku;
    }

    public void setTypBudynku(BuildingType typBudynku)
    {
        this.typBudynku = typBudynku;
    }
}
