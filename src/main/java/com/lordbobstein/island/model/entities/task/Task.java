/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.entities.task;

import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.logic.common.enums.TaskType;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author lordbobstein
 */
@Entity
public class Task implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    private long stageId;
    
    @OneToOne
    private Player player;
    
    /**Typ zadania np. NOWYBUDYNEK, ULEPSZENIE_BUDYNKU
     * 
     */
    @Enumerated(EnumType.STRING)
    private TaskType taskType;
    
    /** nazwa celu - jednostki, budynku.
     * W przypadku budynku jest to nr enum
     * 
     */
    private String targetsName;    
    
    @OneToOne(cascade = CascadeType.ALL)
    private Location location;
    
    /** Czy zadanie zostało juz done
     * 
     */
    private boolean done;

      
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Player getPlayer()
    {
        return player;
    }

    public void setPlayer(Player player)
    {
        this.player = player;
    }

    public String getTargetsName()
    {
        return targetsName;
    }

    public void setTargetsName(String targetsName)
    {
        this.targetsName = targetsName;
    }
    
    public TaskType getTaskType()
    {
        return taskType;
    }

    public void setTaskType(TaskType taskType)
    {
        this.taskType = taskType;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public boolean isDone()
    {
        return done;
    }

    public void setDone(boolean done)
    {
        this.done = done;
    }

    public long getStageId()
    {
        return stageId;
    }

    public void setStageId(long stageId)
    {
        this.stageId = stageId;
    }
}
