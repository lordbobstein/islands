/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.player;

import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.entities.building.Building;
import com.lordbobstein.island.model.entities.island.Island;
import com.lordbobstein.island.model.entities.resource.Resource;
import com.lordbobstein.island.model.entities.unit.Unit;
import com.lordbobstein.island.model.logic.common.enums.BuildingType;
import java.io.Serializable;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

/**
 * Encja gracza
 *
 * @author lordbobstein
 */
@Entity
public class Player implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(cascade = CascadeType.PERSIST)
    private PlayerData playerData;

    @OneToOne(cascade = CascadeType.MERGE)
    private Archipelago archipelago;

    private boolean isBuildingInQueue;

    @OneToOne(cascade = CascadeType.ALL)
    private Resource food;

    @OneToOne(cascade = CascadeType.ALL)
    private Resource wood;

    @OneToOne(cascade = CascadeType.ALL)
    private Resource stone;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE, mappedBy = "owner")
    @OrderBy("id ASC")
    private Set<Unit> units;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @MapKeyEnumerated(EnumType.ORDINAL)    
    @JoinColumn(name = "idTable1")
    private Map<BuildingType, Building> buildings;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE, mappedBy = "owner")
    @OrderBy("id ASC")
    private Set<Island> islands;
    
    @OneToOne
    private Island playersIsland;

    public Player()
    {
    }

    public Player(PlayerData playerData)
    {
        this.playerData = playerData;
        this.food = new Resource();
        this.wood = new Resource(); 
        this.stone = new Resource();
        this.isBuildingInQueue = false;
        this.units = new HashSet<>();
        this.buildings = new EnumMap<>(BuildingType.class);
        this.islands = new HashSet<>();
    }

    public Player(PlayerData playerData, Set<Island> island)
    {
        this.playerData = playerData;
        this.food = new Resource();
        this.wood = new Resource(); 
        this.stone = new Resource();
        this.isBuildingInQueue = false;
        this.units = new HashSet<>();
        this.buildings = new EnumMap<>(BuildingType.class);
        this.islands = island;   
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public PlayerData getPlayerData()
    {
        return playerData;
    }

    public void setPlayerData(PlayerData playerData)
    {
        this.playerData = playerData;
    }

    public Archipelago getArchipelago()
    {
        return archipelago;
    }

    public void setArchipelago(Archipelago archipelago)
    {
        this.archipelago = archipelago;
    }

    public boolean isBuildingInQueue()
    {
        return isBuildingInQueue;
    }

    public void setIsBuildingInQueue(boolean isBuildingInQueue)
    {
        this.isBuildingInQueue = isBuildingInQueue;
    }

    public Resource getFood()
    {
        return food;
    }

    public void setFood(Resource food)
    {
        this.food = food;
    }

    public Resource getWood()
    {
        return wood;
    }

    public void setWood(Resource wood)
    {
        this.wood = wood;
    }

    public Resource getStone()
    {
        return stone;
    }

    public void setStone(Resource stone)
    {
        this.stone = stone;
    }

    public Set<Unit> getUnits()
    {
        return units;
    }

    public void setUnits(Set<Unit> units)
    {
        this.units = units;
    }

    public Map<BuildingType, Building> getBuildings()
    {
        return buildings;
    }

    public void setBuildings(Map<BuildingType, Building> buildings)
    {
        this.buildings = buildings;
    }

    public Set<Island> getIslands()
    {
        return islands;
    }

    public void setIslands(Set<Island> islands)
    {
        this.islands = islands;
    }

    public Island getPlayersIsland()
    {
        return playersIsland;
    }

    public void setPlayersIsland(Island playersIsland)
    {
        this.playersIsland = playersIsland;
    }

    
    
}
