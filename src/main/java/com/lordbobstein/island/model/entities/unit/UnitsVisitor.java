/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.unit;

import com.lordbobstein.island.model.dao.islandsDAO.IslandDAO;
import com.lordbobstein.island.model.dao.playersDAO.PlayerDAO;
import com.lordbobstein.island.model.dao.unitsDAO.UnitDAO;
import com.lordbobstein.island.model.entities.island.Island;
import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.entities.resource.Resource;
import com.lordbobstein.island.model.logic.common.enums.IslandResourcesType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author lordbobstein
 */
@Component
public class UnitsVisitor implements IUnitsVisitor
{

    @Autowired
    private PlayerDAO playerDAO;

    @Autowired
    private IslandDAO islandDAO;

    @Autowired
    private UnitDAO unitDAO;

    @Override
    public void visitPort(Trader unit)
    {
        Player player = unit.getOwner();
        int resBefore;
        Resource resource = null;

        IslandResourcesType type = unit.getResourceType();
        switch (type)
        {
            case FOOD:
                resource = player.getFood();
                break;

            case STONE:
                resource = player.getStone();
                break;

            case WOOD:
                resource = player.getWood();
                break;
        }

        resBefore = resource.getResourceValue();
        resource.setResourceValue(resBefore + unit.getFillingHold());
        playerDAO.save(player);

        Location destinationLocation = unit.getDestinationLocation();

        unit.setFillingHold(0);
        unit.setCurrentLocation(destinationLocation);
        unit.setInMove(false);
        unit.setResourceType(null);
        unit.setRequiredStages(0);

        unitDAO.save(unit);
    }

    @Override
    public void visitResourcesIsland(Trader unit)
    {
        Location destinationLocation = unit.getDestinationLocation();

        unit.setCurrentLocation(destinationLocation);
        unit.setInMove(false);

        Island island = islandDAO.findByLocationXAndLocationY(unit.getCurrentLocation().getX(), unit.getCurrentLocation().getY());

        IslandResourcesType type = island.getResourceType();
        switch (type)
        {
            case FOOD:
                unit.setResourceType(IslandResourcesType.FOOD);
                break;

            case STONE:
                unit.setResourceType(IslandResourcesType.STONE);
                break;

            case WOOD:
                unit.setResourceType(IslandResourcesType.WOOD);
                break;
        }

        unit.setFillingHold(unit.getHoldSize());
        unit.setRequiredStages(0);

        unitDAO.save(unit);
    }

    @Override
    public void visitPort(Warship warship)
    {
        Location destinationLocation = warship.getDestinationLocation();

        warship.setCurrentLocation(destinationLocation);
        warship.setInMove(false);
        warship.setResourceType(null);
        warship.setRequiredStages(0);

        unitDAO.save(warship);
    }

    @Override
    public void visitResourcesIsland(Warship warship)
    {
        Location destinationLocation = warship.getDestinationLocation();

        warship.setCurrentLocation(destinationLocation);
        warship.setInMove(false);
        warship.setRequiredStages(0);

        unitDAO.save(warship);
    }
}
