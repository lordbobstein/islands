/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.unit;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author lordbobstein
 */
@Entity
@DiscriminatorValue("WARSHIP")
public class Warship extends Unit
{

    public Warship()
    {  
        this.costStone = 1000;
        this.costWood = 1250;
        this.costFood = 500;
        this.speed = 30;
    }
    
    @Override
    public void acceptVisitPort(IUnitsVisitor visitor)
    {
        visitor.visitPort(this);
    }

    @Override
    public void acceptResourcesIsland(IUnitsVisitor visitor)
    {
        visitor.visitResourcesIsland(this);
    }
    
}
