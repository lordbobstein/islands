/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.unit;

import com.google.common.math.IntMath;
import com.lordbobstein.island.model.dao.islandsDAO.IslandDAO;
import com.lordbobstein.island.model.dao.unitsDAO.UnitDAO;
import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.entities.island.Island;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.logic.common.enums.IslandResourcesType;
import com.lordbobstein.island.model.logic.common.enums.UnitState;
import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.DiscriminatorOptions;

/**
 *
 * @author lordbobstein
 */
@Entity
@DiscriminatorColumn(name = "unitType", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorOptions(force = true)
public abstract class Unit implements Serializable, VisitableUnit
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @Cascade(CascadeType.ALL)
    protected Location currentLocation;

    @OneToOne
    @Cascade(CascadeType.ALL)
    protected Location destinationLocation;

    @OneToOne
    private Archipelago archipelago;

    @ManyToOne
    private Player owner;

    private boolean inMove;

    protected int requiredStages;

    protected int speed;

    protected int costStone;

    protected int costWood;

    protected int costFood;
    
    @Enumerated(EnumType.STRING)
    protected UnitState UnitState;

    @Enumerated(EnumType.STRING)
    private IslandResourcesType resourceType;

    @Transient
    protected boolean enoughResourcesToBuild;

    @Transient
    public void moveUnit(UnitDAO unitDAO, IslandDAO islandDAO, IUnitsVisitor unitsVisitor)
    {
        if (this.requiredStages == 1)
        {
            int destX = destinationLocation.getX();
            int destY = destinationLocation.getY();

            Island island = islandDAO.findByLocationXAndLocationY(destX, destY);

            //lokalizacja znajduje sie w porcie dlatego wyspa nie ma typu surowcow
            if (island.getResourceType() == null)
            {
                this.acceptVisitPort(unitsVisitor);

            }
            else if (island.getResourceType() != null)
            {
                this.acceptResourcesIsland(unitsVisitor);
            }
        }
        else
        {
            this.requiredStages--;
        }

        unitDAO.save(this);
    }

    @Transient
    public int calculateRequiredStages()
    {
        double distance = this.calculateDistanceFromCurrentLocation();

        return (int) Math.ceil(distance / getSpeed());
    }

    @Transient
    public double calculateDistanceFromCurrentLocation()
    {
        int x = getCurrentLocation().getX() - destinationLocation.getX();
        int y = getCurrentLocation().getY() - destinationLocation.getY();
        x = IntMath.pow(x, 2);
        y = IntMath.pow(y, 2);

        double afterSqrt = Math.sqrt(x + y);
        double result = Math.round(afterSqrt * 10) / 10.0;

        return result;
    }

    //Getters and setters
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Location getCurrentLocation()
    {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation)
    {
        this.currentLocation = currentLocation;
    }

    public Location getDestinationLocation()
    {
        return destinationLocation;
    }

    public void setDestinationLocation(Location destinationLocation)
    {
        this.destinationLocation = destinationLocation;
    }

    public Archipelago getArchipelago()
    {
        return archipelago;
    }

    public void setArchipelago(Archipelago archipelago)
    {
        this.archipelago = archipelago;
    }

    public Player getOwner()
    {
        return owner;
    }

    public void setOwner(Player owner)
    {
        this.owner = owner;
    }

    public boolean isInMove()
    {
        return inMove;
    }

    public void setInMove(boolean inMove)
    {
        this.inMove = inMove;
    }

    public int getRequiredStages()
    {
        return requiredStages;
    }

    public void setRequiredStages(int requiredStages)
    {
        this.requiredStages = requiredStages;
    }

    public int getSpeed()
    {
        return speed;
    }

    public void setSpeed(int speed)
    {
        this.speed = speed;
    }

    public int getCostStone()
    {
        return costStone;
    }

    public void setCostStone(int costStone)
    {
        this.costStone = costStone;
    }

    public int getCostWood()
    {
        return costWood;
    }

    public void setCostWood(int costWood)
    {
        this.costWood = costWood;
    }

    public int getCostFood()
    {
        return costFood;
    }

    public void setCostFood(int costFood)
    {
        this.costFood = costFood;
    }

    public IslandResourcesType getResourceType()
    {
        return resourceType;
    }

    public void setResourceType(IslandResourcesType resourceType)
    {
        this.resourceType = resourceType;
    }

    public boolean isEnoughResourcesToBuild()
    {
        return enoughResourcesToBuild;
    }

    public void setEnoughResourcesToBuild(boolean enoughResourcesToBuild)
    {
        this.enoughResourcesToBuild = enoughResourcesToBuild;
    }

    public UnitState getUnitState()
    {
        return UnitState;
    }

    public void setUnitState(UnitState UnitState)
    {
        this.UnitState = UnitState;
    }
    
    

}
