/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.player;

import com.lordbobstein.island.model.logic.common.enums.PlayerType;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * Encja typu gracza
 *
 * @author lordbobstein
 */
@Entity
public class PlayerData implements Serializable, Cloneable
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "Proszę podać login.")
    @Size(min = 1, max = 10, message = "Login musi posiadać od 1 do 10 znaków")
    private String login;

    @NotEmpty(message = "Proszę podać hasło.")
    @Size(min = 6, max = 10, message = "Hasło musi posiadać od 6 do 10 znaków")
    private String password;
    
    @Enumerated(EnumType.STRING)
    private PlayerType playerType;    

    @Override
    public PlayerData clone() throws CloneNotSupportedException
    {
        PlayerData cloned = (PlayerData) super.clone();
        cloned.setId(null);
        return cloned;
    }
    
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    } 

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public PlayerType getPlayerType()
    {
        return playerType;
    }

    public void setPlayerType(PlayerType playerType)
    {
        this.playerType = playerType;
    }
    
    
    
    

}
