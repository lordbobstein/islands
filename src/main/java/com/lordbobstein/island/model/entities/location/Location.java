/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.location;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author lordbobstein
 */
@Entity
public class Location implements Serializable, Cloneable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    private int x;
    private int y;

    public Location()
    {
    }

    public Location(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    @Override
    public Location clone() throws CloneNotSupportedException
    {
        Location clonedLocation = (Location) super.clone();
        clonedLocation.setId(null);
        return  clonedLocation;        
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public int getX()
    {
        return x;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public int getY()
    {
        return y;
    }

    public void setY(int y)
    {
        this.y = y;
    }
    
}
