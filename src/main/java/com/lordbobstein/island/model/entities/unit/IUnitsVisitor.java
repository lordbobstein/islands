/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.unit;

/**
 *
 * @author lordbobstein
 */
public interface IUnitsVisitor
{
    void visitPort(Trader unit);
    void visitPort(Warship unit);
    
    void visitResourcesIsland(Trader unit);
    void visitResourcesIsland(Warship unit);
}
