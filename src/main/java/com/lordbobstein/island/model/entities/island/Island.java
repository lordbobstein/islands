/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.island;

import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import com.lordbobstein.island.model.entities.location.Location;
import com.lordbobstein.island.model.entities.player.Player;
import com.lordbobstein.island.model.logic.common.enums.IslandResourcesType;
import com.lordbobstein.island.model.logic.common.enums.IslandType;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author lordbobstein
 */
@Entity
public class Island implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)    
    private Integer id;
    
    @OneToOne(cascade = CascadeType.ALL)     
    private Location location;
    
    @ManyToOne       
    private Archipelago archipelago;
    
    @Column(nullable = false)
    private boolean isFree;
    
    @Enumerated(EnumType.STRING)
    private IslandType islandType;
    
    @Enumerated(EnumType.STRING)
    private IslandResourcesType resourceType; 
    
    @ManyToOne()
    private Player owner;

    public Island()
    {
    }

    public Island(Location location, boolean isFree)
    {
        this.location = location;
        this.isFree = isFree;
    }
    
    public Island(Location location, Archipelago archipelago, IslandType islandType, boolean isFree)
    {
        this.location = location;
        this.archipelago = archipelago;
        this.islandType = islandType;
        this.isFree = isFree;
    }
    
    public Island(Location location, Archipelago archipelago, IslandType islandType, IslandResourcesType resourceType, boolean isFree)
    {
        this.location = location;
        this.archipelago = archipelago;
        this.islandType = islandType;
        this.resourceType = resourceType;
        this.isFree = isFree;
    }
    
    public int getArchipelagoId()
    {
        return archipelago.getId();
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public Archipelago getArchipelago()
    {
        return archipelago;
    }

    public void setArchipelago(Archipelago archipelago)
    {
        this.archipelago = archipelago;
    }

    public IslandType getIslandType()
    {
        return islandType;
    }

    public void setIslandType(IslandType islandType)
    {
        this.islandType = islandType;
    }

    public IslandResourcesType getResourceType()
    {
        return resourceType;
    }

    public void setResourceType(IslandResourcesType resourceType)
    {
        this.resourceType = resourceType;
    }

    public boolean isIsFree()
    {
        return isFree;
    }

    public void setIsFree(boolean isFree)
    {
        this.isFree = isFree;
    }

    public Player getOwner()
    {
        return owner;
    }

    public void setOwner(Player owner)
    {
        this.owner = owner;
    }
}
