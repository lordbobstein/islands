/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.unit;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author lordbobstein
 */
@Entity
@DiscriminatorValue("TRADER")
public class Trader extends Unit
{    
    private int holdSize;    
    private int fillingHold;

    public Trader()
    {
        this.holdSize = 250;
        this.costStone = 500;
        this.costWood = 500;
        this.costFood = 500;
        this.speed = 10;
    } 

    public int getHoldSize()
    {
        return holdSize;
    }

    public void setHoldSize(int holdSize)
    {
        this.holdSize = holdSize;
    }

    public int getFillingHold()
    {
        return fillingHold;
    }

    public void setFillingHold(int fillingHold)
    {
        this.fillingHold = fillingHold;
    }

   
    @Override    
    public void acceptVisitPort(IUnitsVisitor visitor)
    {
        visitor.visitPort(this);
    }

    
    @Override
    public void acceptResourcesIsland(IUnitsVisitor visitor)
    {
        visitor.visitResourcesIsland(this);
    }
}
