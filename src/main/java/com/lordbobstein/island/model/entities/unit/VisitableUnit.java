package com.lordbobstein.island.model.entities.unit;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lordbobstein
 */
public interface VisitableUnit
{
    public void acceptVisitPort(IUnitsVisitor visitor);
    public void acceptResourcesIsland(IUnitsVisitor visitor);
}
