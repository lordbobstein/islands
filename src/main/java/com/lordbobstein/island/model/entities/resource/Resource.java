/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.entities.resource;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author lordbobstein
 */
@Entity
public class Resource implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    private int resourceValue;
    private int resourceIncrease;

    public Resource()
    {
        this.resourceValue = 0;
        this.resourceIncrease = 0;
    }

    public Resource(int resourceValue)
    {
        this.resourceValue = resourceValue;
        this.resourceIncrease = 0;
    }

    public void improveIncrease(int value)
    {
        resourceIncrease += value;
    }
    
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getResourceValue()
    {
        return resourceValue;
    }

    public void setResourceValue(int resourceValue)
    {
        this.resourceValue = resourceValue;
    }

    public int getResourceIncrease()
    {
        return resourceIncrease;
    }

    public void setResourceIncrease(int resourceIncrease)
    {
        this.resourceIncrease = resourceIncrease;
    }

    
    
    
}
