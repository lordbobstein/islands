/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.entities.archipelago;

import com.lordbobstein.island.model.entities.island.Island;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author lordbobstein
 */
@Entity
public class Archipelago implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;  
      
    private int numberOfIslands;
    private int stage;

    @OneToMany(mappedBy = "archipelago", fetch = FetchType.LAZY)
    private Set<Island> islandList;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getNumberOfIslands()
    {
        return numberOfIslands;
    }

    public void setNumberOfIslands(int numberOfIslands)
    {
        this.numberOfIslands = numberOfIslands;
    }

    public int getStage()
    {
        return stage;
    }

    public void setStage(int stage)
    {
        this.stage = stage;
    }

    public Set<Island> getIslandList()
    {
        return islandList;
    }

    public void setIslandList(Set<Island> islandList)
    {
        this.islandList = islandList;
    }

    
    
    
}
