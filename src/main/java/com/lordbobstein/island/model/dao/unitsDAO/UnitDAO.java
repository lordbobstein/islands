/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.dao.unitsDAO;

import com.lordbobstein.island.model.entities.unit.Unit;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lordbobstein
 */
@Repository
public interface UnitDAO extends JpaRepository<Unit, Integer>
{
    List<Unit> findByInMove(boolean inMove);
    List<Unit> findByInMoveAndArchipelagoId(boolean inMove, int id);
    
    @Query(value = "select u.* from Unit u, Location l where"
            + " inMove = ?1 and l.id = currentLocation_id and"
            + " l.x = ?2 and l.y = ?3 and u.unitType = 'WARSHIP'", nativeQuery = true)
    List<Unit> findWarshipsByInMoveAndCurrentLocation_xAndCurrentLocation_y(boolean inMove, int x, int y);
    
    @Query(value = "select u.* from Unit u, Location l where"
            + " inMove = ?1 and l.id = currentLocation_id and"
            + " l.x = ?2 and l.y = ?3 and u.unitType = 'TRADER'", nativeQuery = true)
    List<Unit> findTradersByInMoveAndCurrentLocation_xAndCurrentLocation_y(boolean inMove, int x, int y);
    
    @Query(value = "select u.* from Unit u, Location l where"
            + " inMove = ?1 and l.id = currentLocation_id and"
            + " l.x = ?2 and l.y = ?3"
            + " and u.owner_id <>  ?4 ", nativeQuery = true)
    List<Unit> findByInMoveAndCurrentLocation_xAndCurrentLocation_yAndNotOwner_id(boolean inMove, int x, int y, int id);
    
    @Query(value = "select u.* from Unit u, Location l where"
            + " inMove = ?1 and l.id = currentLocation_id and"
            + " l.x = ?2 and l.y = ?3 and u.unitType = 'WARSHIP'"
            + " and u.owner_id = ?4", nativeQuery = true)
    List<Unit> findWarshipsByInMoveAndCurrentLocation_xAndCurrentLocation_yAndOwner_id(boolean inMove, int x, int y, int id);
}
