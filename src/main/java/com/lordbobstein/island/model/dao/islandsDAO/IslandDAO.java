/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.dao.islandsDAO;

import com.lordbobstein.island.model.entities.island.Island;
import com.lordbobstein.island.model.logic.common.enums.IslandType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lordbobstein
 */
@Repository
public interface IslandDAO extends JpaRepository<Island, Integer>
{
    @Query("from Island where id = (select min(id) from Island where islandType = 'PLAYERS_ISLAND' and isFree = 1)")
    Island findFirstFreeIsland();
    
    @Query("from Island i, Location l where i.location = l and l.x = ?1 and l.y = ?2")
    Island findByLocationXAndLocationY(int x, int y);
    
    List<Island> findByArchipelago_IdAndIslandType(int id, IslandType islandType);
}
