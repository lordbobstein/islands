/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.dao.tasksDAO;

import com.lordbobstein.island.model.entities.task.Task;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lordbobstein
 */
@Repository
public interface TaskDAO extends JpaRepository<Task, Integer>
{
    List<Task> findAllByDone(boolean done);
    
    @Query(value = "select t.* from Task t, Player p, Archipelago a where t.player_id = p.id and t.done = false and p.archipelago_id = a.id and a.id = ?1", nativeQuery = true)
    List<Task> findByArchipelago(int archipelagoId);
    
    List<Task> findByPlayer_PlayerData_Login(String login, Pageable pageable);  
    List<Task> findByTargetsName(String targetsName);
}
