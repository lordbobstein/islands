/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.dao.playersDAO;

import com.lordbobstein.island.model.entities.player.PlayerData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lordbobstein
 */
@Repository
public interface PlayerDataDAO extends JpaRepository<PlayerData, Integer>
{
    PlayerData findByLogin(String login);
}
