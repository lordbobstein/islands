/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lordbobstein.island.model.dao.playersDAO;

import com.lordbobstein.island.model.entities.player.Player;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lordbobstein
 */
@Repository
public interface PlayerDAO extends JpaRepository<Player, Integer>
{
    
    Player findByPlayerData_Login(String login);
    
    List<Player> findByArchipelagoId(int archipelagoId);
    
    @Query(value = "select p.* from Player p join (select i.*, "
            + "count(*) as cnt from Island i group by i.owner_id) "
            + "i on (i.owner_id = p.id) having p.archipelago_id = ?1 "
            + "order by i.cnt desc", nativeQuery = true)
    List<Player> findAndSortByNumberOfIsland(int archipelagoId);
}
