/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lordbobstein.island.model.dao.archipelagosDAO;

import com.lordbobstein.island.model.entities.archipelago.Archipelago;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lordbobstein
 */
@Repository
public interface ArchipelagosDAO extends JpaRepository<Archipelago, Integer>
{

    @Query("from Archipelago where id=(select max(id) from Archipelago)")
    Archipelago findLast();

    @Query(value = "select a.stage from Archipelago a, PlayerData pd, Player p where pd.login = ?1 and pd.id = p.playerData_id and p.archipelago_id = a.id", nativeQuery = true)
    int getCurrentStageByPlayerLogin(String login);
    
    @Query(value = "select a.id from Archipelago a, PlayerData pd, Player p where pd.login = ?1 and pd.id = p.playerData_id and p.archipelago_id = a.id", nativeQuery = true)
    int getCurrentArchipelagoIdByPlayerLogin(String login);

    Archipelago findById(int id); // po co?

}
